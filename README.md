# psycholinguistics
The data visualization repo for the paper "Evidence-for-early-comprehension-of-action-verbs" by Iris Nomuku.

### Repo Structure
1. __data/__: Contains all raw and transformed data from the study 
2. __docs/__: Documentation folder contains a legend file for experiment subjects (VP, Versuch Person) and the manuscript paper (manuscript_action-verbs.docx, initial manuscript draft by Iris). 
3. __old_src/__: The old Python scripts are stored here. Can be removed.
4. __results/__: Resulting plots from Python script.
5. __src/__: Contains Python3 script which creates resulting plots from given data.
6. __install.jl__ : Installation script for julia.
7. __REQUIRE__ : Julia deps file.

#### Usage 
After downloading this repo, change directory to the "src" folder. There, within your terminal, type:
```
$ python3 script.py "data/data_output/tables_4500" "data/assignment" "results/"
```
This will create the resulting plots inside the "results/" folder stored as `*.png` output files.


OLD DOCUMENTATION:
After downloading this repo, change directory to the "kern" folder. There, within your terminal, type:

```
$ python3 python_reimplement_3500.py
```

This will create a new "tables_3500" folder within the "data_output" folder with all the `*.tsv` output files. Currently, this script produces tables for the 3500 ms fixed window. For 4000 ms and 4500 ms fixed window, run the `python_reimplement_4000.py` and `python_reimplement_4500.py` programs respectively. 

With every commit a CI runner is triggered which plots the diagrams from "dataVIS" folder.
The plots are stored on the Conquaire server under https://conquaire.uni-bielefeld.de/feedback/GITLAB_PATH_TO_REPO, e.g. https://conquaire.uni-bielefeld.de/feedback/fherrmann/17P-DatViz-RohlfingK-psycholinguistics/.

You can also execute the script `vis_verbs_py.py` inside the "dataVIS" folder by manually executing:

```
$ python3 dataVIS/vis_verbs_py.py -o dataVIS/
```

Optional parameters are "-o" which sets a path for the output plots (else plots are stored in the same directory where the script is executed) and "-l" to print the feedback link inside the CI runner output under CI/CD -> Jobs in the Gitlab interface.

## Partner Info
+ __Discipline__: Linguistics
+ __GitLab__: https://gitlab.ub.uni-bielefeld.de/conquaire/psycholinguistics
+ __Description__: (Child language acquisition):} PD Dr. Katharina Rohlfing studies language acquisition of young children by investigating mother-child interaction during a game and a free play situation. Recorded data includes audio and video recordings from 3 camera perspectives, which will be extensively coded and transcribed. CONQUAIRE will support the sharing of the data with a partner group from the University of Warsaw, Poland, who will act as external validator of the data.
+ __People__: 
    + __PI__: [Prof. Dr. Katharina Rohlfing](https://kw1.uni-paderborn.de/institute-einrichtungen/institut-fuer-germanistik-und-vergleichende-literaturwissenschaft/germanistik/personal/rohlfing/) 
    + __PostDoc__: Dr. Iris Nomuku: <iris.nomikou@port.ac.uk>


## LICENSE
+ COPYRIGHT © 2017 [SVAKSHA][SVAKSHA] All Rights Reserved. 
+ This software and all code snippets used in the documentation files are released under the [AGPLv3 License](http://www.gnu.org/licenses/agpl.html) License as detailed in the `LICENSE.md` file. All copies and forks of this work must retain the Copyright and the Licence file for the source codes along with this permission notice in all copies or substantial portions of the Software.

