import os
import argparse
import numpy as np   # numpy for matrix ops
import pandas as pd     # pandas for XLS reading.
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as mpl
import pylab as pl
import sys

verbs = ['Bauen', 'Fahren', 'Lesen', 'Sitzen', 'Anziehen', 'Baden', 'Essen', 'Schlafen']
verbs_lower = [verb.lower() for verb in verbs]
COL_INDEX_LABEL = 0
COL_INDEX_BEFORE = 1
COL_INDEX_AFTER = 2
COL_INDEX_DIFF = 3


data_dir = sys.argv[1]
data_assignment_path = sys.argv[2]
results_path = sys.argv[3]
# data_dir = repo_dir + "data/data_output/tables_4500"
# data_assignment_path = repo_dir + "data/assignment"
# results_path = repo_dir + "results/"


def extract_average_looking_times(dir_path):
	# initialization
	data = { age : { verb.lower() : { 'before':[], 'after':[], 'diff':[] } for verb in verbs } for age in ['9','10'] }
	print(data)
	
	# read age data
	f = open(os.path.join(data_assignment_path, 'age.csv'))
	age_dict = {}
	for line in f:
		parts = line.strip().split(',')
		age_dict[parts[0]] = parts[1]
	
	
	for file_name in os.listdir(data_dir):
		print('Current file: ' + file_name)
		if not file_name.startswith('VP'): continue
		file_path = os.path.join(data_dir, file_name)
		vp_name = file_name.split('_')[0]

		# open file and read all lines
		f = open(file_path)
		lines = []
		for line in f:
			lines.append(line.strip().split('\t'))
		
		# process verbs
		for i in range(25,55):
			label = lines[i][COL_INDEX_LABEL].lower()
			before = lines[i][COL_INDEX_BEFORE]
			after = lines[i][COL_INDEX_AFTER]
			diff = lines[i][COL_INDEX_DIFF]
			
			if not label.startswith('a_') and not label.startswith('b_'): continue
			if vp_name not in age_dict:
				print('No age entry for ' + vp_name)
				continue
				
			verb = label.split('_')[1]
			age = age_dict[vp_name]
			
			if verb not in verbs_lower: continue
			
			if before != 'nan':
				data[age][verb]['before'].append(float(before))
			if after != 'nan':
				data[age][verb]['after'].append(float(after))
			if diff != 'nan':
				data[age][verb]['diff'].append(float(diff))
			
	return data
			

		
		
def plot_before_after_9month():
	data = extract_average_looking_times(data_dir)
	list_before = [ np.mean(data['9'][verb.lower()]['before']) for verb in verbs ]
	list_after = [ np.mean(data['9'][verb.lower()]['after']) for verb in verbs ] 
	grafverbmp = pd.DataFrame({'percentageBefore': list_before, 'percentageAfter': list_after}, index=verbs)
	#----------------------
	# VISUALISATION Graph  
	#----------------------
	colors = ['b', 'g']
	mpl.rcParams.update({'font.size': 12})
	graf_df = (grafverbmp).plot(kind='bar', stacked=False, color=colors, legend=True)
                        
	mpl.xlabel('VERBS') 
	mpl.ylabel('Mean Looking Time, %')
	legend = graf_df.legend(["Looking time to target before", "Looking time to target after"])
	legend._set_loc(4)
	legend.set_bbox_to_anchor((1.1,1))
	graf_df.figure.subplots_adjust(right=0.9) 
	label = graf_df.get_legend_handles_labels()
	mpl.rcParams.update({'font.size': 12})
	mpl.savefig(os.path.join(results_path, 'fig_before_after_9month.png'), bbox_inches='tight')
	mpl.show()
	mpl.close()
	
	
	
def plot_before_after_10month():
	data = extract_average_looking_times(data_dir)
	list_before = [ np.mean(data['10'][verb.lower()]['before']) for verb in verbs ]
	list_after = [ np.mean(data['10'][verb.lower()]['after']) for verb in verbs ] 
	grafverbmp = pd.DataFrame({'percentageBefore': list_before, 'percentageAfter': list_after}, index=verbs)
	#----------------------
	# VISUALISATION Graph  
	#----------------------
	colors = ['b', 'g']
	graf_df = (grafverbmp).plot(kind='bar', stacked=False, color=colors, legend=True)
                        
	mpl.xlabel('VERBS') 
	mpl.ylabel('Mean Looking Time, %')
	legend = graf_df.legend(["Looking time to target before", "Looking time to target after"])
	legend._set_loc(4)
	legend.set_bbox_to_anchor((1.1,1))
	graf_df.figure.subplots_adjust(right=0.9) 
	label = graf_df.get_legend_handles_labels()
	mpl.rcParams.update({'font.size': 12})
	mpl.savefig(os.path.join(results_path, 'fig_before_after_10month.png'), bbox_inches='tight')
	mpl.show()
	mpl.close()




def plot_before_after_all():
	data = extract_average_looking_times(data_dir)
	list_before = [ np.mean(data['9'][verb.lower()]['before']+data['10'][verb.lower()]['before']) for verb in verbs ]
	list_after = [ np.mean(data['9'][verb.lower()]['after']+data['10'][verb.lower()]['after']) for verb in verbs ] 
	grafverbmp = pd.DataFrame({'percentageBefore': list_before, 'percentageAfter': list_after}, index=verbs)
	#----------------------
	# VISUALISATION Graph  
	#----------------------
	colors = ['b', 'g']
	graf_df = (grafverbmp).plot(kind='bar', stacked=False, color=colors, legend=True)
                        
	mpl.xlabel('VERBS') 
	mpl.ylabel('Mean Looking Time, %')
	legend = graf_df.legend(["Looking time to target before", "Looking time to target after"])
	legend._set_loc(4)
	legend.set_bbox_to_anchor((1.1,1))
	graf_df.figure.subplots_adjust(right=0.9) 
	label = graf_df.get_legend_handles_labels()
	mpl.rcParams.update({'font.size': 12})
	mpl.savefig(os.path.join(results_path, 'fig_before_after_all.png'), bbox_inches='tight')
	mpl.show()
	mpl.close()



def plot_before_after_diff_9_10month():
	data = extract_average_looking_times(data_dir)
	list_before = [ np.mean(data['10'][verb.lower()]['before']) - np.mean(data['9'][verb.lower()]['before']) for verb in verbs ]
	list_after = [ np.mean(data['10'][verb.lower()]['after']) - np.mean(data['9'][verb.lower()]['after']) for verb in verbs ] 
	grafverbmp = pd.DataFrame({'percentageBefore': list_before, 'percentageAfter': list_after}, index=verbs)
	#----------------------
	# VISUALISATION Graph  
	#----------------------
	colors = ['b', 'g']
	graf_df = (grafverbmp).plot(kind='bar', stacked=False, color=colors, legend=True)
                        
	mpl.xlabel('VERBS') 
	mpl.ylabel('Difference in Mean Looking Time, %')
	legend = graf_df.legend(["Looking time to target before", "Looking time to target after"])
	legend._set_loc(4)
	legend.set_bbox_to_anchor((1.1,1))
	graf_df.figure.subplots_adjust(right=0.9) 
	label = graf_df.get_legend_handles_labels()
	mpl.rcParams.update({'font.size': 12})
	mpl.savefig(os.path.join(results_path, 'fig_before_after_diff_9_10month.png'), bbox_inches='tight')
	mpl.show()
	mpl.close()

	
	
	
	
def plot_diff_9month():    
    data = extract_average_looking_times(data_dir)
    differences_list = [ np.mean(data['9'][verb.lower()]['diff']) for verb in verbs ] 
    frame = pd.DataFrame({'diffs' : differences_list}, index=verbs)

    #---------------------
    # visualisation
    #---------------------
    graf_matrx = frame.plot(kind='bar', stacked=False, legend=False)
    mpl.axhline(0, color='black')
    mpl.xlabel('VERBS') 
    mpl.ylabel('Looking Time in %') 
    label = graf_matrx.get_legend_handles_labels()
    mpl.rcParams.update({'font.size': 12})
    plot_name = 'fig_diff_9month.png'
    mpl.savefig(os.path.join(results_path, plot_name), bbox_inches='tight')
    mpl.show()
    mpl.close()




def plot_diff_10month():    
    data = extract_average_looking_times(data_dir)
    differences_list = [ np.mean(data['10'][verb.lower()]['diff']) for verb in verbs ] 
    frame = pd.DataFrame({'diffs' : differences_list}, index=verbs)

    #---------------------
    # visualisation
    #---------------------
    graf_matrx = frame.plot(kind='bar', stacked=False, legend=False)
    mpl.axhline(0, color='black')
    mpl.xlabel('VERBS') 
    mpl.ylabel('Looking Time in %') 
    label = graf_matrx.get_legend_handles_labels()
    mpl.rcParams.update({'font.size': 12})
    plot_name = 'fig_diff_10month.png'
    mpl.savefig(os.path.join(results_path, plot_name), bbox_inches='tight')
    mpl.show()
    mpl.close()
    
    


def plot_diff_all():    
    data = extract_average_looking_times(data_dir)
    differences_list = [ np.mean(data['9'][verb.lower()]['diff'] + data['10'][verb.lower()]['diff']) for verb in verbs ] 
    frame = pd.DataFrame({'diffs' : differences_list}, index=verbs)

    #---------------------
    # visualisation
    #---------------------
    graf_matrx = frame.plot(kind='bar', stacked=False, legend=False)
    mpl.axhline(0, color='black')
    mpl.xlabel('VERBS') 
    mpl.ylabel('Looking Time in %') 
    label = graf_matrx.get_legend_handles_labels()
    mpl.rcParams.update({'font.size': 12})
    plot_name = 'fig_diff_all.png'
    mpl.savefig(os.path.join(results_path, plot_name), bbox_inches='tight')
    mpl.show()
    mpl.close()




def plot_before_after_average():
	data = extract_average_looking_times(data_dir)
	#list_before = []
	list_after = []
	temp_list = []
	
	for verb in verbs:
		temp_list.extend( data['9'][verb.lower()]['after'] )
	list_after.append( np.mean(temp_list) )
	temp_list = []
	
	for verb in verbs:
		temp_list.extend( data['10'][verb.lower()]['after'] )
	list_after.append( np.mean(temp_list) )
	temp_list = []

	
	grafverbmp = pd.DataFrame({'percentageAfter': list_after}, index=['9-month olds', '10-month olds'])
	#----------------------
	# VISUALISATION Graph  
	#----------------------
	colors = ['b', 'g']
	mpl.rcParams.update({'font.size': 12})
	graf_df = (grafverbmp).plot(kind='bar', stacked=False, color=colors, legend=True)
                        
	mpl.xlabel('Age') 
	mpl.ylabel('Mean Looking Time, %')
	legend = graf_df.legend(["Looking time to target after"])
	legend._set_loc(4)
	legend.set_bbox_to_anchor((1.1,1))
	graf_df.figure.subplots_adjust(right=0.9) 
	label = graf_df.get_legend_handles_labels()
	mpl.rcParams.update({'font.size': 12})
	mpl.hlines(0.5, xmin=-100, xmax=100, linewidth=0.7)
	mpl.savefig(os.path.join(results_path, 'fig_after_average.png'), bbox_inches='tight')
	mpl.show()
	mpl.close()


def main():
	plot_before_after_9month()		
	plot_before_after_10month()
	plot_before_after_all()
	plot_before_after_diff_9_10month()
	plot_diff_9month()
	plot_diff_10month()
	plot_diff_all()
	plot_before_after_average()

if __name__ == "__main__":
	main()