################################################################################
# FILE       : install.jl
# DESCRIPTION: Installation file.
# AUTHOR     : SVAKSHA, http://svaksha.com/pages/Bio
# SOURCE     : 
# COPYRIGHT ©: 2017-Now SVAKSHA, All Rights Reserved.
# LICENSE    : GNU AGPLv3 & subject to meeting all the terms in the LICENSE file
# DATES      : 2017Apr21.
################################################################################
#

Pkg.add("ArgParse")
Pkg.add("Debug")
Pkg.add("HTTPClient")
Pkg.add("HttpCommon") 
Pkg.add("JSON")
Pkg.add("LibGit2")
Pkg.add("Requests")
Pkg.add("TimerOutputs")

Pkg.update()

# Load the .juliarc file
#-------------------------------------------------------------------------------
if !isfile(joinpath(homedir(), ".juliarc.jl"))
  fid = open(joinpath(homedir(), ".juliarc.jl"), "w")
  write(fid, "push!(LOAD_PATH, pwd())\n")
  close(fid)
else
  load_path = false
  fid = open(joinpath(homedir(), ".juliarc.jl"), "r")
  while !eof(fid)
    line = readline(fid)
    if contains(line, "LOAD_PATH, pwd())")
      load_path = true
    end
  end
  close(fid)

  if load_path == false
    fid = open(joinpath(homedir(), ".juliarc.jl"), "a")
    write(fid, "push!(LOAD_PATH, pwd())\n")
    close(fid)
  end
end


