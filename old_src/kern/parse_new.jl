#This file holds some functins to reproduce the Data from execelsheet and create some barcharts as visualisation of the data

using DataFrames
using Vega
include("./vpDataType.jl")
## constants
## runs with julia version 0.5.1

const horizontal_window = [0, 960, 1920]
const time_window = 3
#define AOIS=>
const aois = Dict(   ""=> [-1, -1], 
   "B_ESSEN_bauen_mögen1_neu.avi"=> [223, 1006],
   "B_essen_BAUEN_mögen2_neu.avi"=> [223, 1006],
   "A_BAUEN_essen_können1_neu.avi"=> [137, 1021],
   "A_bauen_ESSEN_können2_neu.avi"=> [137, 1021],
   "B_BADEN_fahren_mögen1_neu.avi"=> [226, 1030],
   "B_baden_FAHREN_mögen2_neu.avi"=> [226, 1030],
   "A_FAHREN_baden_können1_neu.avi"=> [189, 993],
   "A_fahren_BADEN_können2_neu.avi"=> [189, 993],
   "A_LESEN_anziehen_können1_neu.avi"=> [176, 1023],
   "A_lesen_ANZIEHEN_können2_neu.avi"=> [176, 1023],
   "B_ANZIEHEN_lesen_mögen1_neu.avi"=> [147, 1093],
   "B_anziehen_LESEN_mögen2_neu.avi"=> [147, 1093],
   "A_SCHAUKELN_trinken_können1_neu.avi"=> [151, 1041],
   "A_schaukeln_TRINKEN_können2_neu.avi"=> [151, 1041],
   "B_TRINKEN_schaukeln_mögen1_neu.avi"=> [213, 1044],
   "B_trinken_SChAUKELN_mögen2_neu.avi"=> [213, 1044],   
   "A_SITZEN_schlafen_können1_neu.avi"=> [207, 993],
   "A_sitzen_SCHLAFEN_können2_neu.avi"=> [207, 993],
   "B_SCHLAFEN_sitzen_mögen1_neu.avi"=> [170, 1026],
   "B_schlafen_SITZEN_mögen2_neu.avi"=> [170, 1026],)


"""
Function which calculates how long the the probant looks right or left for the given line, in the given time window.
Args:
- data_table: The raw data from Eyetracker
- line: the line where to check if the probant looks to left or right.
- area: the key of the aoi for the current line.
Returns:
-returns a tuple with the time the probant looks right or left.
"""

function fixation_in_window(data_table::DataFrames.DataFrame, line::Int64, area::String)
    left = 0
    right = 0
    #isna checks if the table value exsists is equivalent to pythons != ""
    if !isna(data_table[22][line])  && !isna(data_table[21][line]) 
        if aois[area][1] <= parse(Int,data_table[22][line]) <= aois[area][2]
            if horizontal_window[1] <= parse(Int,data_table[21][line]) < horizontal_window[2]
                                left = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
            end
            if horizontal_window[2] <= parse(Int,data_table[21][line]) < horizontal_window[3]
                                right = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
            end
        end
    else
        last_data = -2*time_window
        next_data = 2*time_window
        #changed range from 1-tw,0 to 1-tw,-1 because range border in julia is inclusive in contrast to python where the upper boarder is exclusiv.
        for i in [(1-time_window):-1...]
            if !isna(data_table[22][line + i])  && !isna(data_table[21][line + i]) 
                last_data = i
            end
            #access first column and count elements => numbers of rows
            if (line - i) < length(data_table[1]) && !isna(data_table[22][line - i])  && !isna(data_table[21][line - i])
                next_data = -i
            end
        end
        if (next_data - last_data) <= time_window
            if (aois[area][1] <= parse(Int,data_table[22][line + next_data]) <= aois[area][2]) &&
             (aois[area][1] <= parse(Int,data_table[22][line + last_data]) <= aois[area][2])
                if (horizontal_window[1] <= parse(data_table[21][line + next_data]) < horizontal_window[2]) &&
                 (horizontal_window[1] <= parse(Int,data_table[21][line + last_data]) < horizontal_window[2])
                    left = (parse(Int,data_table[10][line + 1]) - parse(Int,data_table[10][line]))
                end
                if (horizontal_window[2] <= parse(data_table[21][line + next_data]) < horizontal_window[3]) && (horizontal_window[2] <= parse(Int,data_table[21][line + last_data]) <horizontal_window[3])
                    right = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
                end
            end
        end
    end
    return(left,right)
end

"""
This function calculates for an given aoi where in which line the avi was started when the last click happend and when the avi ended.
It returns a triple with the startline, endline and line of last click.
"""
function calcStart_End_Lastclick(data_table::DataFrames.DataFrame,aoi::String)
    start_line = 1
    end_line = 1
    for i in 1:length(data_table[1])
        if !isna(data_table[9][i])
            if data_table[9][i] == aoi && start_line == 1
                start_line = i
            end
            if data_table[9][i] == aoi
                end_line = i
            end
        end
    end
    last_click = -1
    for i in start_line:end_line
        if !isna(data_table[13][i])
                    last_click = i
        end
    end
    return (start_line,end_line,last_click)
end



"""
This function creates and returns a Sample for an given avi file (`aoi`), a table which contains the raw output from eyetracker, start_line, end_line and the last click, which are calculated by `calcStart_End_Lastclick
It returns a Saple which contains:
- the given avi
- the time the probant looked left before the click
- the time the probant looked right before the click
- the time the probant looked left after the click
- the time the probant looked right after the click
- the fixation direction.
"""
function createSampleFromData(aoi::String,data_table::DataFrames.DataFrame,start_line::Int64,end_line::Int64,last_click::Int64)
    left_before_click = 0
    right_before_click = 0
    click_time = parse(Int,data_table[10][last_click]) - parse(Int,data_table[10][start_line])
    # start collect data before click
    for i in start_line:(last_click-1)
        (left,right) = fixation_in_window(data_table,i,aoi)
        left_before_click += left
        right_before_click += right
    end
    # Start collect data after click + 367 ms
    left_after_click = 0
    right_after_click = 0
    after_click_plus_duration = last_click
    while (parse(Int,data_table[10][after_click_plus_duration]) < (parse(Int,data_table[10][last_click]) +367))
        after_click_plus_duration += 1
    end
    for i in after_click_plus_duration:(end_line-1)
        (left,right) = fixation_in_window(data_table,i,aoi)
        left_after_click += left
        right_after_click += right
    end
    # Get the direction of the first registered fixation after the keypress:
    direction_fixation = ""
    first_fixation = after_click_plus_duration
    while first_fixation < end_line
        while ((first_fixation < end_line) && (isna(data_table[21][first_fixation]) || isna(data_table[22][first_fixation])))
            first_fixation += 1
        end
        if !isna(data_table[21][first_fixation]) && !isna(data_table[22][first_fixation])
            if aois[aoi][1] <= parse(Int,data_table[22][first_fixation]) <= aois[aoi][2]
                if 0 <= parse(Int,data_table[21][first_fixation]) < 960
                    direction_fixation = "left"
                    break
                elseif 960 <= parse(Int,data_table[21][first_fixation]) < 1920
                    direction_fixation = "right"
                    break
                else
                    first_fixation += 1
                end
            else
                first_fixation += 1
            end
        end
    end
    return Sample(aoi,left_before_click,left_after_click,right_before_click,right_after_click,direction_fixation)
end

"""
This function creates for each inputfile in the given path (.tsv files) an result table, which is filled withe `createUpperTablePartFromVP` and `createBottomTablePartFromVP`. in the given output path. 
It  returns an Array with all VP-Types, which were used and created while creating the output tables.
"""


function createVPxTsv(inputFolderPath::String,outputFolderPath::String)
    mkpath(outputFolderPath)
    vps = Array{VP,1}()
     for file_name in readdir(inputFolderPath)
        if endswith(file_name,".tsv") && !contains(file_name,"output")
            outputFilePath = outputFolderPath*"/"*split(file_name,".")[1]*"_output.tsv"
            inputfilePath = inputFolderPath * "/" *file_name
            data_table = readtable(inputfilePath, separator = '\t',eltypes = repeat([String],outer = 27))
            vp = VP(file_name)
            FillVP(vp,data_table)
            push!(vps,vp)
            result_file = open(outputFilePath,"w")
            createUpperTablePartFromVP(result_file,vp)
            write(result_file,"\n")
            write(result_file,"\n")
            createBottomTablePartFromVP(result_file,vp)
            close(result_file)
        end
    end
    return vps
end



"""
This function takes an `outputStream` and a `VP Type`. It simply writes the triple from the `orderedSamplesWithAVG` at the given VP to the outputstream.
"""
function createBottomTablePartFromVP(outputStream::IOStream,vp::VP)
    write(outputStream,"\t\tTAR BEF MS\tDIS BES MS\tTAG AFT MS\tDIS AFT MS\tBefore Total\tAfter Total\tTAR BEF PERC\tDIS BEF PERC\tTarget PERC\tDIS PERC\tT-D\t\tDirection\tDirection\tDirection coding\tDS Direction\n")
    for (fst,snd,thrd) in vp.getOrderedSamples()
        writeSampleToFileLower(outputStream,fst)
        writeSampleToFileLower(outputStream,snd)
        writeSampleToFileLower(outputStream,thrd)
    end
end


"""
This is only a help function, for `createBottomTablePartFromVP`, it gets a `sample`,`NoClickSample` or `AVGSample` and an `outputStream` as Arguemnts
and writes the correct tab seperated output to the stream.
"""
    
end
function writeSampleToFileLower(outputStream::IOStream,sample::Union{Sample,NoClickSample,AVGSample})
    if typeof(sample) == NoClickSample
            write(outputStream,"\t$(sample.name)\t"*"-\t"^11 * "\t-\t-\t-\n")
    else
        write(outputStream,"\t$(sample.name)\t$(sample.tar_bef_ms)\t$(sample.dis_bef_ms)\t$(sample.tag_aft_ms)\t$(sample.dis_aft_ms)\t$(sample.before_total)\t$(sample.after_total)\t$(sample.tar_bef_perc)\t$(sample.dis_bef_perc)\t$(sample.target_prec)\t$(sample.dis_perc)\t$(sample.t_d)")
        if typeof(sample) == Sample
            write(outputStream,"\t\t$(sample.fixation_direction)\t$(sample.direction_code)\t$(sample.direction_code)\n")
        elseif typeof(sample) == AVGSample
            write(outputStream,"\t"^5 * "$(sample.ds_direction)\n")
        end
    end
end



"""
This function gets the the `data_table`(eyetracker-raw data) and corresponding VP.
It iterates of the aois and call the `calcStart_End_Lastclick` for each aoi and with this infromation creates all Samples for the VP.
"""
function FillVP(vp::VP,data_table::DataFrames.DataFrame)
    for aoi in sort(collect(keys(aois)))
        startline, endline, lastclick = calcStart_End_Lastclick(data_table,aoi)
        if lastclick == -1
            vp.addSample(NoClickSample(aoi))
        else
            vp.addSample(createSampleFromData(aoi,data_table,startline,endline,lastclick))
        end
    end
end

"""
Simply converts a given `Sample` or `NoClickSample` to text and writes it to the given outputstream.
"""

function writeSampleToFileUpper(outputStream::IOStream,sample::Union{Sample,NoClickSample},vpname::String)
    if typeof(sample) == NoClickSample
        if sample.aoi == ""
            write(outputStream,"$vpname\t$(sample.aoi)\t0\t0\t0\t0\n")
        else
            write(outputStream,"$vpname\t$(sample.aoi)\tNo click registered\n")
        end
    else
        write(outputStream,"$vpname\t$(sample.aoi)\t$(sample.left_before)\t$(sample.left_after)\t$(sample.right_before)\t$(sample.right_after)\t$(sample.fixation_direction)\t$(sample.before_total)\t$(sample.after_total)\t$(sample.prop_before)\t$(sample.prop_target)\t$(sample.prop_target)\t$(sample.prop_distractor)\t$(sample.prop_dis_before)\n")
    end
end


"""
Simply writes all `Samples`
 of the given VP to the given outputStream
"""
function createUpperTablePartFromVP(outputStream::IOStream,vp::VP)
    result_file = outputStream
    write(result_file,"TSV-FILE\tAVI-File\tLeft_before\tLeft_After\tRight_Before\tRight_After\tFixation_Direction\tBefore Total\tAfter total\tProp Before\tProp After\tProp Target\tProp Distractor\tProp Dis(before)\n")
    for sample in vp.samples
        writeSampleToFileUpper(result_file,sample,vp.vpname)
    end
end

"""
This function calculates for each verb the avg before total percent and after total percent and creates an barchart with shows before total average vs after total average for each verb.
The barchart is written to the given outputpath.
"""

function createVerbsbeforeVsVerbsAfter(vps::Array{VP,1},outputpath::String)
    mkpath(outputpath)
    verbsbefore = Dict()
    verbsafter = Dict()
    for vp in vps
        for (_,_,sample) in vp.getOrderedSamples()
            verb = lowercase(sample.name)
            if !haskey(verbsbefore,verb)
                verbsbefore[verb] = Array{Union{Float64,String},1}()
            end
            if !haskey(verbsafter,verb)
                verbsafter[verb] = Array{Union{Float64,String},1}()
            end
            push!(verbsbefore[verb],sample.tar_bef_perc)
            push!(verbsafter[verb],sample.target_prec)
        end
    end
    category = []
    perc = []
    group = []
    for verb in keys(verbsbefore)
        push!(category,verb,verb)
        push!(group,"before")
        push!(group,"after")
        perc_before_mean = mean(filter(v-> !isnan(v) && v ≠ "-",verbsbefore[verb]))
        perc_after_mean = mean(filter(v-> !isnan(v) && v ≠ "-",verbsafter[verb]))
        push!(perc,perc_before_mean)
        push!(perc,perc_after_mean)
    end
    gb = groupedbar(x = category, y = perc, group = group)
    colorscheme!(gb, palette = ["Blue","Orange"])
    xlab!(gb,title = "verbs")
    ylab!(gb,title = "AVG percent")
    html_graph = Vega.genhtml(gb)
    graphout = open(outputpath*"/"*"verbs_before_after.html","w")
    write(graphout,html_graph)
    close(graphout)
end

"""
This function creates barcharts like `createVerbsbeforeVsVerbsAfter` does. But this barcharts are only for one VP and not average.
"""
function createVerbBeforeVsVerbAfterForEachVP(vps::Array{VP,1},outputpath::String)
    mkpath(outputpath)
    for vp in vps   
        category = []
        perc = []
        group = []
        for (_,_,sample) in vp.getOrderedSamples()
            verb = lowercase(sample.name)
            push!(category,verb,verb)
            sample.tar_bef_perc == "-" || isnan(sample.tar_bef_perc) ? push!(perc,0.1) : push!(perc,sample.tar_bef_perc)
            sample.tar_bef_perc == "-" || isnan(sample.tar_bef_perc) ? push!(group,"NaN") : push!(group,"before")
            sample.target_prec == "-" || isnan(sample.target_prec) ? push!(perc,0.1) : push!(perc,sample.target_prec)
            sample.target_prec == "-" || isnan(sample.target_prec) ? push!(group,"NaN") : push!(group,"after")
        end
        gb = groupedbar(x = category, y = perc, group = group)
        xlab!(gb,title = "verbs")
        ylab!(gb,title = "percent")
        html_graph = Vega.genhtml(gb)
        graphout = open(outputpath*"/"*split(vp.vpname,".")[1]*"_verbs_before_after.html","w")
        write(graphout,html_graph)
        close(graphout)
    end
end


vps = createVPxTsv("../data_Rohdaten","../data_output/tables")
createVerbsbeforeVsVerbsAfter(vps,"../data_output/visualisation")
createVerbBeforeVsVerbAfterForEachVP(vps,"../data_output/visualisation")



