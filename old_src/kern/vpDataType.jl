#=
This file contains multiple types with represents some parts of the tables, 
which should be reproduced.
=#

"""
This marcro simply swap the values of two variables.
Arguments
--------------
x
    variable which should be swaped with the other(y)
y
    variable which should be swaped with the other(x)

"""

macro swap(x,y)
   quote
      local tmp = $(esc(x))
      $(esc(x)) = $(esc(y))
      $(esc(y)) = tmp
    end
end

"""
**Sample Type**
Represents the evaluated data from EYE-Tracker for one AVI File... (One of the rows of the upper part of the table...)
It simply stores all values which are created and calculated from rawdata. For more information look at the create sample funtion.
"""
type Sample
    #values for table...
    aoi::String
    left_before::Int64
    left_after::Int64
    right_before::Int64
    right_after::Int64
    fixation_direction::String
    before_total::Int64
    after_total::Int64
    prop_before::Float64
    prop_target::Float64
    prop_after::Float64
    prop_distractor::Float64
    prop_dis_before::Float64
    a_or_b::Char
    correctWordIsLeft::Bool
    correctWord::String
    tar_bef_ms::Float64
    dis_bef_ms::Float64
    tag_aft_ms::Float64
    dis_aft_ms::Float64
    correct_direction::String
    tar_bef_perc::Float64
    dis_bef_perc::Float64
    target_prec::Float64
    dis_perc::Float64
    name::String
    t_d::Union{Float64,String}
    direction_code::Union{Int64,String}
    getAoi::Function
    getCorrectWord::Function
    function Sample(aoi::String,left_before::Int64,left_after::Int64,right_before::Int64,right_after::Int64,fixation_direction::String)
        this = new()
        this.aoi = aoi
        this.left_before = left_before
        this.left_after = left_after
        this.right_before = right_before
        this.right_after = right_after
        this.fixation_direction = fixation_direction
        this.before_total = left_before + right_before
        this.after_total = right_after + left_after
        this.a_or_b = aoi[1]
        words = split(aoi,"_")
        this.correctWord = ""
        try                                          #uppercase because of SCHaUKLEN
            this.correctWord =  isupper(words[2])  ? words[2] : uppercase(words[3])
            this.correctWordIsLeft =  isupper(words[2])
        catch
            this.correctWord = ""
        end
        this.prop_before = ((this.correctWordIsLeft ? this.left_before : this.right_before)/ this.before_total)
        this.prop_after = ((this.correctWordIsLeft ? this.left_after : this.right_after) / this.after_total)
        this.prop_target = this.prop_after
        this.prop_distractor = ((!this.correctWordIsLeft ? this.left_after : this.right_after) / this.after_total)
        this.prop_dis_before = ((!this.correctWordIsLeft  ? this.left_before : this.right_before) / this.before_total)
        this.tar_bef_perc = this.prop_before
        this.dis_bef_perc = this.prop_dis_before
        this.target_prec = this.prop_after
        this.dis_perc = this.prop_distractor
        this.name = "$(this.a_or_b)_$(this.correctWord)"
        this.tar_bef_ms = this.correctWordIsLeft ? this.left_before : this.right_before
        this.dis_bef_ms = this.correctWordIsLeft ? this.right_before : this.left_before
        this.tag_aft_ms = this.correctWordIsLeft ? this.left_after : this.right_after
        this.dis_aft_ms = this.correctWordIsLeft ? this.right_after : this.left_after
        this.correct_direction = this.correctWordIsLeft ? "left" : "right"
        if this.correctWordIsLeft && this.correctWord == "BAUEN"
            this.tar_bef_ms = this.left_after
            this.dis_bef_ms = this.right_after
        end
        this.t_d = "-"
        try 
            this.t_d = this.target_prec - this.dis_perc
        catch
            this.t_d = "-"
        end
        if this.fixation_direction != "" && !isnan(this.t_d)
            this.direction_code = this.fixation_direction == this.correct_direction ? 1 : 0
        else
            this.direction_code = "-"
        end
        this.getAoi = function getAoi()
            return this.aoi
        end
        this.getCorrectWord = function getCorrectWord()
            return this.correctWord
        end
        return this
    end
end


"""
**NoClickSample Type**
This Datatype is a lightweight version of `Sample`. It stores all Data which is saved when there is no Click registered in the experiment.
"""
type NoClickSample
    aoi::String
    a_or_b::Char
    correctWord::String
    name::String
    getAoi::Function
    getCorrectWord::Function
    function NoClickSample(aoi::String)
        this = new()
        if aoi == ""
            this.aoi = ""
            this.a_or_b = ' '
            this.correctWord = ""
            this.name = ""
        else
            this.aoi = aoi
            words = split(aoi,"_")
            this.correctWord = ""
                try                                          #uppercase because of SCHaUKLEN
                    this.correctWord =  isupper(words[2])  ? words[2] : uppercase(words[3])
                catch
                    this.correctWord = ""
                end
            this.a_or_b = aoi[1]
            this.name = "$(this.a_or_b)_$(this.correctWord)"
        end
        this.getAoi = function getAoi()
            return this.aoi
        end
        this.getCorrectWord = function getCorrectWord()
            return this.correctWord
        end
        return this
    end
end

"""
**AVGSample Type**
this type stores the result of the average calculation for a pair of samples. 
Simply give two Samples to the contructor and it will calculate the average with help from the `meanOfValidData` Function.
"""
type AVGSample
    name::String
    tar_bef_ms::Float64
    dis_bef_ms::Float64
    tag_aft_ms::Float64
    dis_aft_ms::Float64
    before_total::Float64
    after_total::Float64
    tar_bef_perc::Float64
    dis_bef_perc::Float64
    target_prec::Float64
    dis_perc::Float64
    t_d::Float64
    ds_direction::Union{Float64,String,Int64}
    function AVGSample(aSample::Union{Sample,NoClickSample},bSample::Union{Sample,NoClickSample})
        validSample = nothing
        this = new()
        if typeof(aSample) == NoClickSample
            if typeof(bSample) != NoClickSample
                validSample = bSample 
            else
                this.name = aSample.correctWord
                this.tar_bef_ms = NaN
                this.dis_bef_ms = NaN
                this.tag_aft_ms = NaN
                this.dis_aft_ms = NaN
                this.before_total =NaN
                this.after_total = NaN
                this.tar_bef_perc =NaN
                this.dis_bef_perc = NaN
                this.target_prec = NaN
                this.dis_perc = NaN
                this.t_d = NaN
                this.ds_direction = "-"
                return this
            end
        elseif typeof(bSample) == NoClickSample
            validSample = aSample
        end
        if validSample != nothing
            this.name = validSample.correctWord
            this.tar_bef_ms = validSample.tar_bef_ms
            this.dis_bef_ms = validSample.dis_bef_ms
            this.tag_aft_ms = validSample.tag_aft_ms
            this.dis_aft_ms = validSample.dis_aft_ms
            this.before_total = validSample.before_total
            this.after_total = validSample.before_total
            this.tar_bef_perc = validSample.tar_bef_perc
            this.dis_bef_perc = validSample.dis_bef_perc
            this.target_prec = validSample.target_prec
            this.dis_perc = validSample.dis_perc
            this.t_d = validSample.t_d
            this.ds_direction = "-"
            if validSample.direction_code != "-"
                this.ds_direction = (validSample.direction_code - (1 - validSample.direction_code))
            else
                this.ds_direction = "-"
            end
        else
            this.name = aSample.correctWord
            this.tar_bef_ms = meanOfValidData(aSample.tar_bef_ms,bSample.tar_bef_ms)
            this.tar_bef_perc = meanOfValidData(aSample.tar_bef_perc,bSample.tar_bef_perc)
            this.dis_bef_ms = meanOfValidData(aSample.dis_bef_ms,bSample.dis_bef_ms)
            this.tag_aft_ms = meanOfValidData(aSample.tag_aft_ms,bSample.tag_aft_ms)
            this.dis_aft_ms = meanOfValidData(aSample.dis_aft_ms,bSample.dis_aft_ms)
            this.before_total = meanOfValidData(aSample.before_total,bSample.before_total)
            this.after_total = meanOfValidData(aSample.after_total,bSample.after_total)
            this.tar_bef_perc = meanOfValidData(aSample.tar_bef_perc,bSample.tar_bef_perc)
            this.dis_bef_perc = meanOfValidData(aSample.dis_bef_perc,bSample.dis_bef_perc)
            this.target_prec = meanOfValidData(aSample.target_prec,bSample.target_prec)
            this.dis_perc = meanOfValidData(aSample.dis_perc,bSample.dis_perc)
            this.t_d = meanOfValidData(aSample.t_d,bSample.t_d)
            maxDirectionCode = 0
            direction_correct = 0
            this.ds_direction = "-"
            if aSample.direction_code != "-"
                maxDirectionCode += 1
                direction_correct += aSample.direction_code
            end
            if bSample.direction_code != "-"
                maxDirectionCode += 1
                direction_correct += bSample.direction_code
            end
            this.ds_direction = (direction_correct -(maxDirectionCode -direction_correct)) / maxDirectionCode

        end
        return this
    end
end

"""
**VP-Datatype**
This type can hold multiple samples. It basically represents one of the result excel tables for one VP.

It has 3 fields and 2 Functions:
Fields:
- `vpname` is simply the name of the proband for identification reasons.
- `samples`is an array of all samples which belongs to this VP were created from the eyetracker rawdata (Samples and NoClickSamples).
- `orderedSamplesWithAVG` is an array of triple. Each triple contents two samples which are the booth tries for one verb. The third value is an sample with represents the average of the first and second one.

Functions:

- `addSample` this function add's one Sample to the VP's samples field. It is used while reading in rawdata from eyetracker.
- `getOrderedSamples` this function fills the orderedSamplesWithAVG and returns it value, so it basically orders all samples and calculate the average value for every pair.
"""
type VP
    vpname::String
    samples::Array{Union{Sample,NoClickSample},1}
    addSample::Function
    orderedSamplesWithAVG::Array{Tuple{Union{Sample,NoClickSample},Union{Sample,NoClickSample},AVGSample},1}
    getOrderedSamples::Function
    function VP(name::String)
        this = new()
        this.vpname = name
        this.samples = Array{Union{Sample,NoClickSample},1}()
        this.orderedSamplesWithAVG = Array{Tuple{Union{Sample,NoClickSample},Union{Sample,NoClickSample},AVGSample},1}()
        this.addSample = function addSample(sample::Union{Sample,NoClickSample})
            push!(this.samples,sample)
        end
        this.getOrderedSamples = function getOrderedSamples()
            if length(this.orderedSamplesWithAVG) == length(this.samples) / 2 && length(this.orderedSamplesWithAVG) > 0
                return this.orderedSamplesWithAVG 
            end
            this.orderedSamplesWithAVG = Array{Tuple{Union{Sample,NoClickSample},Union{Sample,NoClickSample},AVGSample},1}()
            workCopy = copy(this.samples)
            filter!(e -> e.getAoi() ≠ "",workCopy)
            while length(workCopy) > 0
                fstElem = pop!(workCopy)
                secElem = pop!(filter(e-> lowercase(e.getCorrectWord()) == lowercase(fstElem.getCorrectWord()),workCopy))
                if fstElem.a_or_b == 'B'
                    @swap(fstElem,secElem)
                end
                newElem = (fstElem,secElem,AVGSample(fstElem,secElem))
                push!(this.orderedSamplesWithAVG,newElem)
                filter!(e->lowercase(e.getCorrectWord()) ≠ lowercase(fstElem.getCorrectWord()),workCopy)
            end
            return this.orderedSamplesWithAVG
        end
        return this
    end
end


"""
**meanOfValidData function**

This fuction takes two arguments `v1` and `v2`, which are the two values you to compute the mean of.
If `v1` and `v2` are valid numbers this function computes simply the mean and returns it. If only one value is valid, only the valid one will be returned.
"""
function meanOfValidData(v1,v2)
    #check if one 
    if v1 == "-" || isnan(v1)
        return(v2)
    elseif v2 == "-" || isnan(v2)
        return(v1)
    else
        return((v1+v2)/2)
    end
end

