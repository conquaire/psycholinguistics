#!/usr/bin/python3

from collections import defaultdict
import distutils.dir_util
import os, csv
import sys

## This script runs with python 3.6

aois = {
   "": [-1, -1],
   "B_ESSEN_bauen_mögen1_neu.avi": [223, 1006],
   "B_essen_BAUEN_mögen2_neu.avi": [223, 1006],
   "A_BAUEN_essen_können1_neu.avi": [137, 1021],
   "A_bauen_ESSEN_können2_neu.avi": [137, 1021],
   "B_BADEN_fahren_mögen1_neu.avi": [226, 1030],
   "B_baden_FAHREN_mögen2_neu.avi": [226, 1030],
   "A_FAHREN_baden_können1_neu.avi": [189, 993],
   "A_fahren_BADEN_können2_neu.avi": [189, 993],
   "A_LESEN_anziehen_können1_neu.avi": [176, 1023],
   "A_lesen_ANZIEHEN_können2_neu.avi": [176, 1023],
   "B_ANZIEHEN_lesen_mögen1_neu.avi": [147, 1093],
   "B_anziehen_LESEN_mögen2_neu.avi": [147, 1093],
   "A_SCHAUKELN_trinken_können1_neu.avi": [151, 1041],
   "A_schaukeln_TRINKEN_können2_neu.avi": [151, 1041],
   "B_TRINKEN_schaukeln_mögen1_neu.avi": [213, 1044],
   "B_trinken_SChAUKELN_mögen2_neu.avi": [213, 1044],
   "A_SITZEN_schlafen_können1_neu.avi": [207, 993],
   "A_sitzen_SCHLAFEN_können2_neu.avi": [207, 993],
   "B_SCHLAFEN_sitzen_mögen1_neu.avi": [170, 1026],
   "B_schlafen_SITZEN_mögen2_neu.avi": [170, 1026]
   }

"""
This dict stores for each avi file the vertical area of interest.
"""

experimentLength = 3500


horizontal_window = [0, 960, 1920]
"""
List values are left border, central_line, right border
"""

time_window = 3
"""
 sliding time window - as often data points are missing we defined a short time
 window during which it is searched for the next fixation data:
 Data is only used when the fixation before the missing data point and after the missing
 data point is pointing into the same direction. Is given in number of steps (3 = 50 ms)
"""


def fixation_in_window(data_table, line, area):
    """
    Function which calculates how long the the probant looks right or left for the given line, in the given time window.
    Args:
        data_table: The raw data from Eyetracker
        line: the line where to check if the probant looks to left or right.
        area: the key of the aoi for the current line.
    Returns:
        returns a tuple with the time the probant looks right or left.

    """
    left = 0
    right = 0
    if data_table[line][21] != '' and data_table[line][20] != '':
        if aois[area][0] <= int(data_table[line][21]) <= aois[area][1]:
            if horizontal_window[0] <= int(data_table[line][20]) < horizontal_window[1]:
                left = (int(data_table[line+1][9]) - int(data_table[line][9]))
            if horizontal_window[1] <= int(data_table[line][20]) < horizontal_window[2]:
                right = (int(data_table[line+1][9]) - int(data_table[line][9]))
    # Check in the time_window if data is just dropped for a short time
    else:
        # Look at time before the current time step and see if data is available
        last_data = -2*time_window
        next_data = 2*time_window
        for i in range((1-time_window), 0):
            if data_table[line + i][21] != '' and data_table[line + i][20] != '':
                last_data = i
            if (line-i)<len(data_table) and data_table[line - i][21] != '' and data_table[line - i][20] != '':
                next_data = -i
        if (next_data - last_data) <= time_window:
            if (aois[area][0] <= int(data_table[line+next_data][21]) <= aois[area][1]) and \
                    (aois[area][0] <= int(data_table[line+last_data][21]) <= aois[area][1]):
                if (horizontal_window[0] <= int(data_table[line+next_data][20]) < horizontal_window[1]) and \
                        (horizontal_window[0] <= int(data_table[line+last_data][20]) < horizontal_window[1]):
                    left = (int(data_table[line+1][9]) - int(data_table[line][9]))
                if (horizontal_window[1] <= int(data_table[line+next_data][20]) < horizontal_window[2]) and \
                        (horizontal_window[1] <= int(data_table[line+last_data][20]) < horizontal_window[2]):
                    right = (int(data_table[line+1][9]) - int(data_table[line][9]))
    return(left, right)


def calcStartEndLastClick(data_table,aoi):
	"""
	This function calculates for an given aoi where in which line the avi was started when the last click happend and when the avi ended.
	It returns a triple with the startline, endline and line of last click.
	"""
	start_line = 0
	end_line = 0
	# Get the data for one aoi experiment
	for i in range(0,len(data_table)):
		if data_table[i][8]==aoi and start_line == 0:
			start_line = i
		if data_table[i][8]==aoi:
			end_line = i
	# find the last click
	last_click = -1
	for i in range(start_line, end_line):
		if data_table[i][12]!='':
			last_click = i
	return(start_line,end_line,last_click)

def fillVP(VP,data_table):
	"""
	This function gets the the `data_table`(eyetracker-raw data) and corresponding VP.
	It iterates of the aois and call the `calcStart_End_Lastclick` for each aoi and with this infromation creates all Samples for the VP.
	"""
	for aoi in sorted(aois):
		startline, endline, lastclick = calcStartEndLastClick(data_table,aoi)
		if lastclick == -1:
			VP.addSample(Sample(aoi,no_click_registered=True))
		else:
			VP.addSample(createSampleFromData(aoi,data_table,startline,endline,lastclick))

def createSampleFromData(aoi,data_table,start_line,end_line,last_click):
	"""
	This function creates and returns a Sample for an given avi file (`aoi`),
    a table which contains the raw output from eyetracker, start_line, end_line
    and the last click, which are calculated by 'calcStart_End_Lastclick'
	It returns a Sample which contains:
	- the given avi
	- the time the probant looked left before the click
	- the time the probant looked right before the click
	- the time the probant looked left after the click
	- the time the probant looked right after the click
	- the fixation direction.
	"""
	left_before_click = 0
	right_before_click = 0

	click_time = int(data_table[last_click][9]) - int(data_table[start_line][9])
	# print(aoi, " - ", click_time)

	currentLine = start_line
	# Start collect data before click:
	for i in range(start_line, last_click):
		# Gaze is in the y window
		(left, right) = fixation_in_window(data_table, i, aoi)
		left_before_click += left
		right_before_click += right

    # Start collect data after click + 367 ms
	left_after_click = 0
	right_after_click = 0
	after_click_plus_duration = last_click
	while (int(data_table[after_click_plus_duration][9]) < (int(data_table[last_click][9]) + 367)):
		after_click_plus_duration += 1
	currentLine = after_click_plus_duration
	while(currentLine < end_line and int(data_table[currentLine][9]) < (int(data_table[after_click_plus_duration][9]) + (experimentLength - 367))):
	# Gaze is in the y window
		(left, right) = fixation_in_window(data_table, currentLine, aoi)
		left_after_click += left
		right_after_click += right
		currentLine += 1

	# Get the direction of the first registered fixation after the keypress:
	direction_fixation = ""
	first_fixation = after_click_plus_duration
	while (first_fixation < end_line):
		while ((first_fixation < end_line) and (
			(data_table[first_fixation][20] == '') or (data_table[first_fixation][21] == ''))):
			first_fixation += 1
		if ((data_table[first_fixation][20] != '') and (data_table[first_fixation][21] != '')):
			if aois[aoi][0] <= int(data_table[first_fixation][21]) <= aois[aoi][1]:
				if 0 <= int(data_table[first_fixation][20]) < 960:
					direction_fixation = "left"
					break
				elif 960 <= int(data_table[first_fixation][20]) < 1920:
					direction_fixation = "right"
					break
				else:
					first_fixation += 1
			else:
				first_fixation += 1
	return (Sample(aoi,left_before_click,left_after_click,right_before_click,right_after_click,direction_fixation))



def createVPsAndTables(inputFolderPath,outputFolderPath):
	"""
	This function reads in all rawdatafiles which are given in inputfolderpath. Creates a VP for each input file and evaluate the raw data and write each VP back to the output path.
	"""
	vps = []
	distutils.dir_util.mkpath(outputFolderPath)
	for file_name in os.listdir(inputFolderPath):
		if file_name.endswith(".tsv"):
			outputFilePath = outputFolderPath +"/" + file_name.split(".")[0] +"_output.tsv"
			inputFilePath = inputFolderPath +"/" + file_name
			data_table = list(csv.reader(open(inputFilePath), delimiter='\t'))
			vp = VP(file_name)
			fillVP(vp,data_table)
			vp.addMissingValues()
			f = open(outputFilePath,"w")
			f.write(vp.upperTablePart())
			f.write("\n\n")
			f.write(vp.lowerTablePart())
			f.close()


class Sample:
	"""
	This class is simply a wrapper for a dict, which represent the result of one avi file (aoi). The methods are alle self explaining.
	"""
	def __init__(self,aoi,left_before = None,left_after = None,right_before = None,right_after = None,fixation_direction = None,no_click_registered = False):
		self.data = dict()
		self.data["aoi"] = aoi
		self.data["no_click_registered"] = no_click_registered
		if aoi == "":
			return
		words = aoi.split("_")										#Because auf SCHaUKLEN it's has to set upper
		self.data["correctWord"] = words[1]  if words[1].isupper() else words[2].upper()
		self.data["a_or_b"] = words[0]
		self.data["name"] = "_".join((words[0],self.data["correctWord"]))
		self.data["correctWordIsLeft"] = words[1].isupper()
		self.data["bef_aft_tar"] = "nan"
		if not no_click_registered:
			self.data["left_before"] = left_before
			self.data["right_before"] = right_before
			self.data["right_after"] = right_after
			self.data["left_after"] = left_after
			self.data["fixation_direction"] = fixation_direction
			self.data["before_total"] = left_before + right_before
			self.data["after_total"] = right_after + left_after
			if self.data["before_total"] > 0:
				self.data["prop_before"] = (left_before if self.data["correctWordIsLeft"] else right_before)/self.data["before_total"]
				self.data["prop_dis_before"] = (right_before if self.data["correctWordIsLeft"] else left_before) / self.data["before_total"]
			else:
				self.data["prop_before"] = "nan"
				self.data["prop_dis_before"] = "nan"
			if self.data["after_total"] > 0:
				self.data["prop_after"] = (left_after if self.data["correctWordIsLeft"] else right_after)/self.data["after_total"]
				self.data["prop_target"] = (left_after if self.data["correctWordIsLeft"] else right_after)/self.data["after_total"]
				self.data["prop_distractor"] = (right_after if self.data["correctWordIsLeft"] else left_after)/self.data["after_total"]
			else:
				self.data["prop_after"] = "nan"
				self.data["prop_target"] = "nan"
				self.data["prop_distractor"] = "nan"
			self.data["before"] = self.data["prop_before"]
			self.data["after"] = self.data["prop_after"]
			if self.data["after"] != "nan" and self.data["before"] != "nan":
				self.data["bef_aft_tar"] = self.data["after"] - self.data["before"]
			self.data["target"] = self.data["prop_target"]


	def __str__(self):
		if self.data["aoi"] == "": return "".join((self.data["aoi"],"\t0"*4,"\n"))
		if self.data["no_click_registered"]: return "".join((self.data["aoi"],"\t No click registered\n"))
		keys = ["aoi","left_before","left_after","right_before","right_after","fixation_direction","before_total","after_total","prop_before","prop_target","prop_target","prop_distractor","prop_dis_before"]
		values = map(str,[self.data[k] for k in  keys])
		return ("\t".join(values) + "\n").replace("\tinf\t","\tnan\t")

	def getAoi(self):
		return self.data["aoi"]

	def getCorrectWord(self):
		return self.data["correctWord"]

	def getData(self):
		return self.data

	def getBefAftTar(self):
		return self.data["bef_aft_tar"]

	def isNoClickRegistered(self):
		return self.data["no_click_registered"]

	def getDisBeforeAfter(self):
		if not self.isNoClickRegistered():
			return (self.data["prop_dis_before"],self.data["prop_distractor"])
		else:
			return ("nan","nan")

	def __lt__(self, other):
		return self.getCorrectWord() < other.getCorrectWord()

	def __le__(self, other):
		return self.getCorrectWord() <= other.getCorrectWord()

	def __eq__(self, other):
		return self.getCorrectWord() == other.getCorrectWord()

	def __gt__(self, other):
		return self.getCorrectWord() > other.getCorrectWord()

	def __ge__(self, other):
		return self.getCorrectWord() >= other.getCorrectWord()

	def addMissingValues(self, disBefore, disAfter):
		self.data["dis_before"] = disBefore
		self.data["dis_after"] = disAfter
		if disAfter == "nan" or disBefore == "nan":
			self.data["before_after_dis"] = "nan"
		else:
			self.data["before_after_dis"] = disAfter - disBefore
		if self.isNoClickRegistered():
			self.data["td"] = "nan"
			self.data["td_ba"] = "nan"
		else:
			if self.data["target"] == "nan" or disAfter == "nan":
				self.data["td"] = "nan"
			else:
				self.data["td"] = self.data["target"] - disAfter
			if self.data["bef_aft_tar"] == "nan" or self.data["before_after_dis"] == "nan":
				self.data["td_ba"] = "nan"
			else:
				self.data["td_ba"] = self.data["bef_aft_tar"] - self.data["before_after_dis"]

	def getLowerTableString(self):
		if self.isNoClickRegistered():
			return "\t" + self.data["a_or_b"] + "_" + self.getCorrectWord() + "\t" + "\t".join(("nan","nan","nan","nan",str(self.data["dis_before"]),str(self.data["dis_after"]),str(self.data["before_after_dis"]),"nan","nan")) + "\n"
		else:
			return "\t" + self.data["a_or_b"] + "_" + self.getCorrectWord() + "\t" + ("\t".join((str(self.data["before"]),str(self.data["after"]),str(self.data["bef_aft_tar"]),str(self.data["target"]),str(self.data["dis_before"]),str(self.data["dis_after"]),str(self.data["before_after_dis"]),str(self.data["td"]),str(self.data["td_ba"])))) + "\n"



class VP():
	"""
	VP Class: This class can hold multiple samples. It basically represents one
              of the result excel tables for one VP. Most of the functions are self explaining. The other one have seperate docs.
	"""
	def __init__(self,Name):
		self.name = Name
		self.data_list = []
		#self.data = defaultdict(list)

	def getName(self):
		return self.name

	def addSample(self,sample):
		self.data_list.append(sample)
		#self.data[sample.getCorrectWord].append(sample)

	def upperTablePart(self):
		"""
		generates the uppertable output
		"""
		return ("TSV-FILE\tAVI-File\tLeft_before\tLeft_After\tRight_Before\tRight_After\tFixation_Direction\tBefore Total\tAfter total\tProp Before\tProp After\tProp Target\tProp Distractor\tProp Dis(before)\n" + self.name + "\t" + (self.name + "\t").join(map(str, self.data_list))).replace("\tinf","\tnan")

	def lowerTablePart(self):
		"""
		generates the lowertable output
		"""
		return("\t\tBefore\tAfter\tBef_Aft_Tar\tTarget\tDis (before)\tDis (after)\tBef_Atf_Dis\tT-D\tT-D(B-A)\n"+"".join(self.getOrderedSamplesText()))

	def addMissingValues(self):
		"""
		For Each Sample this function searches the inverse Sample(same avi but other word is correct) and calculate the difference score for the current sample.
		"""
		for aktElem in self.data_list:
			if aktElem.getAoi() == "": continue
			for e in self.data_list:
				if e.getAoi().lower().split("_")[:2] == aktElem.getAoi().lower().split("_")[:2] and e.getCorrectWord() != aktElem.getCorrectWord():
					aktElem.addMissingValues(*e.getDisBeforeAfter())
					break

	def _meanOfValidData(self,value1,value2):
		"""
		This function calculate the mean of two values if both are valid numbers, if not it returns the valid number.
		"""
		if value1 == "-"  or value1 == "nan":
			return(value2)
		if value2 == "-"  or value2 == "nan":
			return(value1)
		return ((value1+value2)/2)

	def getOrderedSamplesText(self):
		"""
		Sort the samples after correct words and add calculate the means for the lower table part.
		"""
		ret = []
		workData = self.data_list
		workData = list(filter(lambda x: x.getAoi() != "",workData))
		workData.sort()
		while workData:
			elem = []
			elem.append(workData.pop(0))
			elem.append(workData.pop(0))
			#swap if B is the first element...
			if elem[0].getAoi()[0] == 'B': elem[0],elem[1] = elem[1],elem[0]
			ret.append(elem[0].getLowerTableString().replace("inf","nan").replace("-nan","nan"))
			ret.append(elem[1].getLowerTableString().replace("inf","nan").replace("-nan","nan"))
			meanStr = "\t" + elem[0].getCorrectWord().upper() + "\t"*3+ str(self._meanOfValidData(elem[0].getBefAftTar(),elem[1].getBefAftTar())) + "\t"*4 + str(self._meanOfValidData(elem[0].getData()["td"],elem[1].getData()["td"])) +"\t"+ str(self._meanOfValidData(elem[0].getData()["td_ba"],elem[1].getData()["td_ba"])) + "\n"
			ret.append(meanStr)
		return ret


def main():
	if(len(sys.argv) > 2):
		"Please only give one argument for the time after keypress"
		return
	elif(len(sys.argv) == 2):
		global experimentLength
		experimentLength = int(sys.argv[1])
	createVPsAndTables("../data_Rohdaten","../data_output/tables_3500")
	print ("Created VP and Tables in the '~/data_output/tables_3500' folder. Program End.")

if __name__ == "__main__":
    main()
