#this file creates the VPxx tables from the excelcheet.
## needed to read in tsv file
using DataFrames
using DataStructures
using Vega
#define AOIS=>
aois = Dict(   ""=> [-1, -1], 
   "B_ESSEN_bauen_mögen1_neu.avi"=> [223, 1006],
   "B_essen_BAUEN_mögen2_neu.avi"=> [223, 1006],
   "A_BAUEN_essen_können1_neu.avi"=> [137, 1021],
   "A_bauen_ESSEN_können2_neu.avi"=> [137, 1021],
   "B_BADEN_fahren_mögen1_neu.avi"=> [226, 1030],
   "B_baden_FAHREN_mögen2_neu.avi"=> [226, 1030],
   "A_FAHREN_baden_können1_neu.avi"=> [189, 993],
   "A_fahren_BADEN_können2_neu.avi"=> [189, 993],
   "A_LESEN_anziehen_können1_neu.avi"=> [176, 1023],
   "A_lesen_ANZIEHEN_können2_neu.avi"=> [176, 1023],
   "B_ANZIEHEN_lesen_mögen1_neu.avi"=> [147, 1093],
   "B_anziehen_LESEN_mögen2_neu.avi"=> [147, 1093],
   "A_SCHAUKELN_trinken_können1_neu.avi"=> [151, 1041],
   "A_schaukeln_TRINKEN_können2_neu.avi"=> [151, 1041],
   "B_TRINKEN_schaukeln_mögen1_neu.avi"=> [213, 1044],
   "B_trinken_SChAUKELN_mögen2_neu.avi"=> [213, 1044],   
   "A_SITZEN_schlafen_können1_neu.avi"=> [207, 993],
   "A_sitzen_SCHLAFEN_können2_neu.avi"=> [207, 993],
   "B_SCHLAFEN_sitzen_mögen1_neu.avi"=> [170, 1026],
   "B_schlafen_SITZEN_mögen2_neu.avi"=> [170, 1026])




horizontal_window = [0, 960, 1920]
time_window = 3
#this in this diect the values of barcharts created :)
verbPercsForVisu = Dict()
verbVsVPVisu = Dict()
#result_file = open("results_csv.txt","w")
function fixation_in_window(data_table, line, area)
    left = 0
    right = 0
    #isna checks if the table value exsists is equivalent to pythons != ""
    if !isna(data_table[22][line])  && !isna(data_table[21][line]) 
        if aois[area][1] <= parse(Int,data_table[22][line]) <= aois[area][2]
            if horizontal_window[1] <= parse(Int,data_table[21][line]) < horizontal_window[2]
                                left = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
            end
            if horizontal_window[2] <= parse(Int,data_table[21][line]) < horizontal_window[3]
                                right = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
            end
        end
    else
        last_data = -2*time_window
        next_data = 2*time_window
        #changed range from 1-tw,0 to 1-tw,-1 because range border in julia is inclusive in contrast to python where the upper boarder is exclusiv.
        for i in [(1-time_window):-1...]
            if !isna(data_table[22][line + i])  && !isna(data_table[21][line + i]) 
                last_data = i
            end
            #access first column and count elements => numbers of rows
            if (line - i) < length(data_table[1]) && !isna(data_table[22][line - i])  && !isna(data_table[21][line - i])
                next_data = -i
            end
        end
        if (next_data - last_data) <= time_window
            if (aois[area][1] <= parse(Int,data_table[22][line + next_data]) <= aois[area][2]) &&
             (aois[area][1] <= parse(Int,data_table[22][line + last_data]) <= aois[area][2])
                if (horizontal_window[1] <= parse(data_table[21][line + next_data]) < horizontal_window[2]) &&
                 (horizontal_window[1] <= parse(Int,data_table[21][line + last_data]) < horizontal_window[2])
                    left = (parse(Int,data_table[10][line + 1]) - parse(Int,data_table[10][line]))
                end
                if (horizontal_window[2] <= parse(data_table[21][line + next_data]) < horizontal_window[3]) && (horizontal_window[2] <= parse(Int,data_table[21][line + last_data]) <horizontal_window[3])
                    right = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
                end
            end
        end
    end
    return(left,right)
end


#computes mean of v1,v2 if v1 or v2 == - it returns the valid one...
function meanOfValidData(v1,v2)
    #check if one 
    if v1 == "-" || isnan(v1)
        return(v2)
    elseif v2 == "-" || isnan(v2)
        return(v1)
    else
        return((v1+v2)/2)
    end
end

for file_name in readdir(pwd())
    values_for_lowerpart = DefaultDict(Dict)
    if endswith(file_name,".tsv") && !contains(file_name,"output")
        result_file = open(split(file_name,".")[1]*"_output.tsv","w")
        write(result_file,"TSV-FILE\tAVI-File\tLeft_before\tLeft_After\tRight_Before\tRight_After\tFixation_Direction\tBefore Total\tAfter total\tProp Before\tProp After\tProp Target\tProp Distractor\tProp Dis(before)\n")
        data_table = readtable(file_name, separator = '\t',eltypes = repeat([String],outer = 27))
        for aoi in sort(collect(keys(aois)))
            start_line = 1
            end_line = 1
            for i in 1:length(data_table[1])
                if !isna(data_table[9][i])
                    if data_table[9][i] == aoi && start_line == 1
                        start_line = i
                    end
                    if data_table[9][i] == aoi
                        end_line = i
                    end
                end
            end
            last_click = -1
            for i in start_line:end_line
                if !isna(data_table[13][i])
                    last_click = i
                end
            end
            if last_click < 0
                write(result_file,"$file_name\t$aoi\tNo click registered\n")
            else
                left_before_click = 0
                right_before_click = 0
                click_time = parse(Int,data_table[10][last_click]) - parse(Int,data_table[10][start_line])
                # start collect data before click
                for i in start_line:(last_click-1)
                    (left,right) = fixation_in_window(data_table,i,aoi)
                    left_before_click += left
                    right_before_click += right
                end
                # Start collect data after click + 367 ms
                left_after_click = 0
                right_after_click = 0
                after_click_plus_duration = last_click
                while (parse(Int,data_table[10][after_click_plus_duration]) < (parse(Int,data_table[10][last_click]) +367))
                    after_click_plus_duration += 1
                end
                for i in after_click_plus_duration:(end_line-1)
                    (left,right) = fixation_in_window(data_table,i,aoi)
                    left_after_click += left
                    right_after_click += right
                end
                # Get the direction of the first registered fixation after the keypress:
                direction_fixation = ""
                first_fixation = after_click_plus_duration
                while first_fixation < end_line
                    while ((first_fixation < end_line) && (isna(data_table[21][first_fixation]) || isna(data_table[22][first_fixation])))
                        first_fixation += 1
                    end
                    if !isna(data_table[21][first_fixation]) && !isna(data_table[22][first_fixation])
                        if aois[aoi][1] <= parse(Int,data_table[22][first_fixation]) <= aois[aoi][2]
                                if 0 <= parse(Int,data_table[21][first_fixation]) < 960
                                    direction_fixation = "left"
                                    break
                                elseif 960 <= parse(Int,data_table[21][first_fixation]) < 1920
                                    direction_fixation = "right"
                                    break
                                else
                                    first_fixation += 1
                                end
                        else
                            first_fixation += 1
                        end
                    end
                end
                #same equations as in the excel sheet
                A_or_B = aoi[1]
                word_Pos = 0 
                words = split(aoi,"_")
                word = ""
                try                                          #uppercase because of SCHaUKLEN
                    word =  isupper(words[2])  ? words[2] : uppercase(words[3])
                    word_Pos =  isupper(words[2])  ? 1 : 2
                catch
                    word = ""
                end
                before_total = left_before_click + right_before_click
                after_total = right_after_click + left_after_click
                prop_before = ((word_Pos == 1 ? left_before_click : right_before_click)/ before_total)
                prop_after = ((word_Pos == 1 ? left_after_click : right_after_click) / after_total)
                prop_target = prop_after
                prop_distractor = ((word_Pos == 2 ? left_after_click : right_after_click) / after_total)
                prop_dis_before = ((word_Pos == 2 ? left_before_click : right_before_click) / before_total)

                write(result_file,"$file_name\t$aoi\t$left_before_click\t$left_after_click\t$right_before_click\t$right_after_click\t$direction_fixation\t$before_total\t$after_total\t$prop_before\t$prop_after\t$prop_target\t$prop_distractor\t$prop_dis_before\n")
                v = Dict()
                #wegen komischen tabellen werten if abfrage
                #save values for each file name for lower table part....
                if word_Pos == 1
                    if word == "BAUEN" 
                        v["tar_bef_ms"] = left_after_click
                        v["dis_bef_ms"] = right_after_click
                    else
                        v["tar_bef_ms"] = left_before_click  
                        v["dis_bef_ms"] = right_before_click  
                    end
                    v["tag_aft_ms"] = left_after_click
                    v["dis_aft_ms"] = right_after_click
                    v["correct_direction"] = "left"

                else
                    v["tar_bef_ms"] = right_before_click 
                    v["dis_bef_ms"] = left_before_click 
                    v["tag_aft_ms"] = right_after_click
                    v["dis_aft_ms"] = left_after_click 
                    v["correct_direction"] = "right"
                end
                v["before_total"] = before_total
                v["after_total"] = after_total
                v["tar_bef_perc"] = prop_before
                v["dis_bef_perc"] = prop_dis_before
                v["target_prec"] = prop_after
                v["dis_perc"] = prop_distractor
                v["direction"] = direction_fixation
                v["name"] = "$(A_or_B)_$word"
                values_for_lowerpart[word][A_or_B] = v
            end
        end
        write(result_file,"\n")
    
        #write lower table part..
        write(result_file,"\n")
        write(result_file,"\t\tTAR BEF MS\tDIS BES MS\tTAG AFT MS\tDIS AFT MS\tBefore Total\tAfter Total\tTAR BEF PERC\tDIS BEF PERC\tTarget PERC\tDIS PERC\tT-D\t\tDirection\tDirection\tDirection coding\tDS Direction\n")

        #calcules the means for the verbs for the lower part of the table...
        for w in collect(keys(values_for_lowerpart))
            #check if values are vaild 
            if w == ""
                continue
            end
            valuesA = nothing
            valuesB = nothing
            try
                valuesA = values_for_lowerpart[w]['A']
            catch
                valuesA = DefaultDict("-")
            end
            try
                valuesB = values_for_lowerpart[w]['B']
            catch
                valuesB = DefaultDict("-")
            end
            #write A


            name = lowercase(w)
            tar_bef_ms_A = valuesA["tar_bef_ms"]
            dis_bef_ms_A = valuesA["dis_bef_ms"]
            tag_aft_ms_A = valuesA["tag_aft_ms"] 
            dis_aft_ms_A = valuesA["dis_aft_ms"] 
            before_total_A = valuesA["before_total"]
            after_total_A = valuesA["after_total"]
            tar_bef_perc_A = valuesA["tar_bef_perc"]
            dis_bef_perc_A = valuesA["dis_bef_perc"]
            target_prec_A = valuesA["target_prec"] 
            dis_perc_A = valuesA["dis_perc"]
            directionA = valuesA["direction"] 
            t_d_A = nothing
            try
                t_d_A = target_prec_A - dis_perc_A
            catch
                t_d_A = "-"
            end
            if directionA != "-" && !isnan(t_d_A)
                direction_code_A = directionA == valuesA["correct_direction"] ? 1 : 0
            else
                direction_code_A = "-"
            end

            write(result_file,"\tA_$name\t$tar_bef_ms_A\t$dis_bef_ms_A\t$tag_aft_ms_A\t$dis_aft_ms_A\t$before_total_A\t$after_total_A\t$tar_bef_perc_A\t$dis_bef_perc_A\t$target_prec_A\t$dis_perc_A\t$t_d_A\t\t$directionA\t$direction_code_A\t$direction_code_A\n")
            #write B

            tar_bef_ms_B = valuesB["tar_bef_ms"]
            dis_bef_ms_B = valuesB["dis_bef_ms"]
            tag_aft_ms_B = valuesB["tag_aft_ms"] 
            dis_aft_ms_B = valuesB["dis_aft_ms"] 
            before_total_B = valuesB["before_total"]
            after_total_B = valuesB["after_total"]
            tar_bef_perc_B = valuesB["tar_bef_perc"]
            dis_bef_perc_B = valuesB["dis_bef_perc"]
            target_prec_B = valuesB["target_prec"] 
            dis_perc_B = valuesB["dis_perc"]
            directionB = valuesB["direction"] 
            t_d_B = nothing
            try
                t_d_B = target_prec_B - dis_perc_B
            catch
                t_d_B = "-"
            end
            if directionB != "-" && !isnan(t_d_B)
                direction_code_B = directionB == valuesB["correct_direction"] ? 1 : 0
            else
                direction_code_B = "-"
            end
            write(result_file,"\tB_$name\t$tar_bef_ms_B\t$dis_bef_ms_B\t$tag_aft_ms_B\t$dis_aft_ms_B\t$before_total_B\t$after_total_B\t$tar_bef_perc_B\t$dis_bef_perc_B\t$target_prec_B\t$dis_perc_B\t$t_d_B\t\t$directionB\t$direction_code_B\t$direction_code_B\n")
            #write means and so on
            word_upper = uppercase(name)
            tar_bef_ms_mean = meanOfValidData(tar_bef_ms_A,tar_bef_ms_B)
            dis_bef_ms_mean = meanOfValidData(dis_bef_ms_A,dis_bef_ms_B)
            tag_aft_ms_mean = meanOfValidData(tag_aft_ms_A,tag_aft_ms_B)
            dis_aft_ms_mean = meanOfValidData(dis_aft_ms_A,dis_aft_ms_B)
            before_total_mean = meanOfValidData(before_total_A,before_total_B)
            after_total_mean = meanOfValidData(after_total_A,after_total_B)
            tar_bef_perc_mean = meanOfValidData(tar_bef_perc_A,tar_bef_perc_B)
            dis_bef_perc_mean = meanOfValidData(dis_bef_perc_A,dis_bef_perc_B)
            target_prec_mean = meanOfValidData(target_prec_A,target_prec_B)
            dis_perc_mean = meanOfValidData(dis_perc_A,dis_perc_B)
            t_d_mean = meanOfValidData(t_d_A,t_d_B)
            #calc ds_direction ...
            Ds_Direction_max = 0
            direction_correct_Code = 0
            if direction_code_A != "-" && !isnan(t_d_A)   
                Ds_Direction_max+=1
                direction_correct_Code += direction_code_A
            else
                direction_code_A = "-"
            end
            if direction_code_B != "-" && !isnan(t_d_B)
                Ds_Direction_max+=1
                direction_correct_Code += direction_code_B
            else
                direction_code_B = "-"
            end
            #Note handle when direction is wether left or right is missing
            ds_direction = (direction_correct_Code - (Ds_Direction_max - direction_correct_Code)) / Ds_Direction_max
            ##fill dict's with data which is nessesary for visualisation...
            if(!haskey(verbPercsForVisu,name))
                verbPercsForVisu[name] = Dict()
                verbPercsForVisu[name]["perc_before"] = []
                verbPercsForVisu[name]["perc_after"] = []
            end
            if tar_bef_perc_mean != "-"
                push!(verbPercsForVisu[name]["perc_before"],tar_bef_perc_mean)
            end
            if target_prec_mean != "-"
                push!(verbPercsForVisu[name]["perc_after"],target_prec_mean)
            end
            #create Data for vp vs verb
            VPx = split(file_name,".")[1]
            if(!haskey(verbVsVPVisu,VPx)
                verbVsVPVisu[VPx] = Dict()
                verbVsVPVisu[VPx][name] = Dict()
            elseif(!haskey(verbVsVPVisu[VPx]),name)
                verbVsVPVisu[VPx][name] = Dict()
            end
                verbVsVPVisu[VPx][name]["before_ms"] = tar_bef_ms_mean
                verbVsVPVisu[VPx][name]["after_ms"] = tag_aft_ms_mean
                verbVsVPVisu[VPx][name]["perc_before"] = tar_bef_perc_mean
                verbVsVPVisu[VPx][name]["perc_after"] = target_prec_mean
            # write lower part of the table to file.... (the mean for a verb)
            write(result_file,"\t$word_upper\t$tar_bef_ms_mean\t$dis_bef_ms_mean\t$tag_aft_ms_mean\t$dis_aft_ms_mean\t$before_total_mean\t$after_total_mean\t$tar_bef_perc_mean\t$dis_bef_perc_mean\t$target_prec_mean\t$dis_perc_mean\t$t_d_mean\t\t\t\t\t$ds_direction\n")
        end
        close(result_file,)
    end
end

#create the group bar chart with before and after perc...
category = []
perc = []
group = []
for (key,value) in verbPercsForVisu
    word = key
    #remove non valid values and build mean over values
    perc_after_mean = mean(filter(x->!isnan(x),value["perc_after"]))
    perc_before_mean = mean(filter(x->!isnan(x),value["perc_before"]))
    push!(category,word,word)
    push!(perc,perc_before_mean)
    push!(perc,perc_after_mean)
    push!(group,"before")
    push!(group,"after")
end
#vg = VegaVisualization()
gb = groupedbar(x = category, y = perc, group = group)
colorscheme!(gb, palette = ["Blue","Orange"])
xlab!(gb,title = "verbs")
ylab!(gb,title = "AVG percent")
html_graph = Vega.genhtml(gb)
graphout = open("graph_output.html","w")
write(graphout,html_graph)
close(graphout)

