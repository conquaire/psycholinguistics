from collections import defaultdict
import distutils.dir_util
import os, csv

## This script runs with julia 0.5.1

aois = {
   "": [-1, -1], 
   "B_ESSEN_bauen_mögen1_neu.avi": [223, 1006],
   "B_essen_BAUEN_mögen2_neu.avi": [223, 1006],
   "A_BAUEN_essen_können1_neu.avi": [137, 1021],
   "A_bauen_ESSEN_können2_neu.avi": [137, 1021],
   "B_BADEN_fahren_mögen1_neu.avi": [226, 1030],
   "B_baden_FAHREN_mögen2_neu.avi": [226, 1030],
   "A_FAHREN_baden_können1_neu.avi": [189, 993],
   "A_fahren_BADEN_können2_neu.avi": [189, 993],
   "A_LESEN_anziehen_können1_neu.avi": [176, 1023],
   "A_lesen_ANZIEHEN_können2_neu.avi": [176, 1023],
   "B_ANZIEHEN_lesen_mögen1_neu.avi": [147, 1093],
   "B_anziehen_LESEN_mögen2_neu.avi": [147, 1093],
   "A_SCHAUKELN_trinken_können1_neu.avi": [151, 1041],
   "A_schaukeln_TRINKEN_können2_neu.avi": [151, 1041],
   "B_TRINKEN_schaukeln_mögen1_neu.avi": [213, 1044],
   "B_trinken_SChAUKELN_mögen2_neu.avi": [213, 1044],   
   "A_SITZEN_schlafen_können1_neu.avi": [207, 993],
   "A_sitzen_SCHLAFEN_können2_neu.avi": [207, 993],
   "B_SCHLAFEN_sitzen_mögen1_neu.avi": [170, 1026],
   "B_schlafen_SITZEN_mögen2_neu.avi": [170, 1026]
   }

"""
This dict stores for each avi file the vertical area of interess.
"""

horizontal_window = [0, 960, 1920]
"""
List values are left boarder, central_line, right boarder
"""


time_window = 3
"""
 sliding time window - as often data points are missing we defined a short time
 window during which it is searched for the next fixation data:
 Data is only used when the fixation before the missing data point and after the missing
 data point is pointing into the same direction. Is given in number of steps (3 = 50 ms)
"""


def fixation_in_window(data_table, line, area):
    """
    Function which calculates how long the the probant looks right or left for the given line, in the given time window.
    Args:
        data_table: The raw data from Eyetracker
        line: the line where to check if the probant looks to left or right.
        area: the key of the aoi for the current line.
    Returns:
        returns a tuple with the time the probant looks right or left.

    """
    left = 0
    right = 0
    if data_table[line][21] != '' and data_table[line][20] != '':
        if aois[area][0] <= int(data_table[line][21]) <= aois[area][1]:
            if horizontal_window[0] <= int(data_table[line][20]) < horizontal_window[1]:
                left = (int(data_table[line+1][9]) - int(data_table[line][9]))
            if horizontal_window[1] <= int(data_table[line][20]) < horizontal_window[2]:
                right = (int(data_table[line+1][9]) - int(data_table[line][9]))
    # Check in the time_window if data is just dropped for a short time
    else:
        # Look at time before the current time step and see if data is available
        last_data = -2*time_window
        next_data = 2*time_window
        for i in range((1-time_window), 0):
            if data_table[line + i][21] != '' and data_table[line + i][20] != '':
                last_data = i   
            if (line-i)<len(data_table) and data_table[line - i][21] != '' and data_table[line - i][20] != '':
                next_data = -i
        if (next_data - last_data) <= time_window:
            if (aois[area][0] <= int(data_table[line+next_data][21]) <= aois[area][1]) and \
                    (aois[area][0] <= int(data_table[line+last_data][21]) <= aois[area][1]):
                if (horizontal_window[0] <= int(data_table[line+next_data][20]) < horizontal_window[1]) and \
                        (horizontal_window[0] <= int(data_table[line+last_data][20]) < horizontal_window[1]):
                    left = (int(data_table[line+1][9]) - int(data_table[line][9]))
                if (horizontal_window[1] <= int(data_table[line+next_data][20]) < horizontal_window[2]) and \
                        (horizontal_window[1] <= int(data_table[line+last_data][20]) < horizontal_window[2]):
                    right = (int(data_table[line+1][9]) - int(data_table[line][9]))
    return(left, right)


def calcStartEndLastClick(data_table,aoi):
	"""
	This function calculates for an given aoi where in which line the avi was started when the last click happend and when the avi ended.
	It returns a triple with the startline, endline and line of last click.
	"""
	start_line = 0
	end_line = 0
	# Get the data for one aoi experiment
	for i in range(0,len(data_table)):
		if data_table[i][8]==aoi and start_line == 0:
			start_line = i
		if data_table[i][8]==aoi:
			end_line = i
	# find the last click
	last_click = -1
	for i in range(start_line, end_line):
		if data_table[i][12]!='':
			last_click = i
	return(start_line,end_line,last_click)

def fillVP(VP,data_table):
	"""
	This function gets the the `data_table`(eyetracker-raw data) and corresponding VP.
	It iterates of the aois and call the `calcStart_End_Lastclick` for each aoi and with this infromation creates all Samples for the VP.
	"""
	for aoi in sorted(aois):
		startline, endline, lastclick = calcStartEndLastClick(data_table,aoi)
		if lastclick == -1:
			VP.addSample(Sample(aoi,no_click_registered=True))
		else:
			VP.addSample(createSampleFromData(aoi,data_table,startline,endline,lastclick))

def createSampleFromData(aoi,data_table,start_line,end_line,last_click):
	"""
	This function creates and returns a Sample for an given avi file (`aoi`), a table which contains the raw output from eyetracker, start_line, end_line and the last click, which are calculated by `calcStart_End_Lastclick
	It returns a Saple which contains:
	- the given avi
	- the time the probant looked left before the click
	- the time the probant looked right before the click
	- the time the probant looked left after the click
	- the time the probant looked right after the click
	- the fixation direction.
	"""
	left_before_click = 0
	right_before_click = 0

	click_time = int(data_table[last_click][9]) - int(data_table[start_line][9])
	# print(aoi, " - ", click_time)

	# Start collect data before click:
	for i in range(start_line, last_click):
		# Gaze is in the y window
		(left, right) = fixation_in_window(data_table, i, aoi)
		left_before_click += left
		right_before_click += right

	# Start collect data after click + 367 ms
	left_after_click = 0
	right_after_click = 0
	after_click_plus_duration = last_click
	while (int(data_table[after_click_plus_duration][9]) < (int(data_table[last_click][9]) + 367)):
		after_click_plus_duration += 1
	for i in range(after_click_plus_duration, end_line):
		# Gaze is in the y window
		(left, right) = fixation_in_window(data_table, i, aoi)
		left_after_click += left
		right_after_click += right

	# Get the direction of the first registered fixation after the keypress:
	direction_fixation = ""
	first_fixation = after_click_plus_duration
	while (first_fixation < end_line):
		while ((first_fixation < end_line) and (
			(data_table[first_fixation][20] == '') or (data_table[first_fixation][21] == ''))):
			first_fixation += 1
		if ((data_table[first_fixation][20] != '') and (data_table[first_fixation][21] != '')):
			if aois[aoi][0] <= int(data_table[first_fixation][21]) <= aois[aoi][1]:
				if 0 <= int(data_table[first_fixation][20]) < 960:
					direction_fixation = "left"
					break
				elif 960 <= int(data_table[first_fixation][20]) < 1920:
					direction_fixation = "right"
					break
				else:
					first_fixation += 1
			else:
				first_fixation += 1
	return (Sample(aoi,left_before_click,left_after_click,right_before_click,right_after_click,direction_fixation))



def createVPsAndTables(inputFolderPath,outputFolderPath):
	vps = []
	distutils.dir_util.mkpath(outputFolderPath)
	for file_name in os.listdir(inputFolderPath):
		if file_name.endswith(".tsv"): 
			outputFilePath = outputFolderPath +"/" + file_name.split(".")[0] +"_output.tsv"
			inputFilePath = inputFolderPath +"/" + file_name
			data_table = list(csv.reader(open(inputFilePath), delimiter='\t'))
			vp = VP(file_name)
			fillVP(vp,data_table)

			f = open(outputFilePath,"w")
			f.write(vp.upperTablePart())
			f.close()
			break;



class Sample:
	def __init__(self,aoi,left_before = None,left_after = None,right_before = None,right_after = None,fixation_direction = None,no_click_registered = False):
		self.data = dict()
		self.data["aoi"] = aoi
		self.data["no_click_registered"] = no_click_registered
		if aoi == "":
			return
		words = aoi.split("_")										#Because auf SCHaUKLEN it's has to set upper
		self.data["correctWord"] = words[1]  if words[1].isupper() else words[2].upper()
		self.data["a_or_b"] = words[0]
		self.data["name"] = "_".join((words[0],self.data["correctWord"]))
		self.data["correctWordIsLeft"] = words[1].isupper()
		if not no_click_registered:
			self.data["left_before"] = left_before
			self.data["right_before"] = right_before
			self.data["right_after"] = right_after
			self.data["left_after"] = left_after
			self.data["fixation_direction"] = fixation_direction
			self.data["before_total"] = left_before + right_before
			self.data["after_total"] = right_after + left_after
			if self.data["before_total"] > 0:
				self.data["prop_before"] = (left_before if self.data["correctWordIsLeft"] else right_before)/self.data["before_total"]
				self.data["prop_dis_before"] = (right_before if self.data["correctWordIsLeft"] else left_before) / self.data["before_total"]
			else:
				self.data["prop_before"] = float("inf")
				self.data["prop_dis_before"] = float("inf")
			if self.data["after_total"] > 0:
				self.data["prop_after"] = (left_after if self.data["correctWordIsLeft"] else right_after)/self.data["after_total"]
				self.data["prop_target"] = (left_after if self.data["correctWordIsLeft"] else right_after)/self.data["after_total"]
				self.data["prop_distractor"] = (right_after if self.data["correctWordIsLeft"] else left_after)/self.data["after_total"]
			else:
				self.data["prop_after"] = float("inf")
				self.data["prop_target"] = float("inf")
				self.data["prop_distractor"] = float("inf")
			self.data["tar_bef_perc"] = self.data["prop_before"]
			self.data["dis_bef_per"] = self.data["prop_dis_before"]
			self.data["target_prec"] = self.data["prop_after"]
			self.data["dis_perc"] = self.data["prop_distractor"]
			self.data["tar_bef_ms"] = left_before if self.data["correctWordIsLeft"] else right_before
			self.data["dis_bef_ms"] = right_before if self.data["correctWordIsLeft"] else left_before
			self.data["tag_aft_ms"] = left_after if self.data["correctWordIsLeft"] else right_after
			self.data["dis_aft_ms"] = right_after if self.data["correctWordIsLeft"] else left_after
			self.data["correct_direction"] = "left" if self.data["correctWordIsLeft"] else "right"
			if self.data["correctWordIsLeft"] and self.data["correctWord"] == "BAUEN":
				self.data["tar_bef_ms"] = left_after
				self.data["dis_bef_ms"] = right_after
			self.data["t_d"] = self.data["target_prec"] - self.data["dis_perc"]
			if self.data["fixation_direction"] != "" and self.data["t_d"] != float("inf"):
				self.data["direction_code"] = 1 if self.data["fixation_direction"] == self.data["correct_direction"] else 0
			else:
				self.data["direction_code"] = "-"

	def __str__(self):
		if self.data["aoi"] == "": return "".join((self.data["aoi"],"\t0"*4,"\n"))
		if self.data["no_click_registered"]: return "".join((self.data["aoi"],"\t No click registered\n"))
		keys = ["aoi","left_before","left_after","right_before","right_after","fixation_direction","before_total","after_total","prop_before","prop_target","prop_target","prop_distractor","prop_dis_before"]
		values = map(str,[self.data[k] for k in  keys])
		return ("\t".join(values) + "\n").replace("\tinf\t","\tnan\t")

	def getAoi(self):
		return self.data["aoi"]

	def getCorrectWord(self):
		return self.data["correctWord"]

	def getData(self):
		return self.data()

	def isNoClickRegistered(self):
		return self.data["no_click_registered"]

class VP():
	def __init__(self,Name):
		self.name = Name
		self.data_list = []
		#self.data = defaultdict(list)

	def getName(self):
		return self.name

	def addSample(self,sample):
		self.data_list.append(sample)
		#self.data[sample.getCorrectWord].append(sample)

	def upperTablePart(self):
		return ("TSV-FILE\tAVI-File\tLeft_before\tLeft_After\tRight_Before\tRight_After\tFixation_Direction\tBefore Total\tAfter total\tProp Before\tProp After\tProp Target\tProp Distractor\tProp Dis(before)\n" + self.name + "\t" + (self.name + "\t").join(map(str, self.data_list))).replace("\tinf","\tnan")

class avgSample():
	def _meanOfValidData(self,value1,value2):
		if value1 == "-" or value1 == float("inf"):
			return(value2)
		if value2 == "-" or value2 == float("inf"):
			return(value1)
		return ((value1+value2)/2)

	def __init__(self,fstSample,sndSample):
		self.data = dict()
		if fstSample.isNoClickRegistered() or snd.isNoClickRegistered:
			validSample = None
			if not fstSample.isNoClickRegistered():
				validSample = fstSample
			elif not sndSample.isNoClickRegistered:
				validSample = sndSample
			else:
				self.data["name"] = fstSample.getCorrectWord()
				self.data["tar_bef_ms"] = float("nan")
				self.data["dis_bef_ms"] = float("nan")
				self.data["tag_aft_ms"] = float("nan")
				self.data["dis_aft_ms"] = float("nan")
				self.data["before_total"] = float("nan")
				self.data["after_total"] = float("nan")
				self.data["tar_bef_perc"] = float("nan")
				self.data["dis_bef_perc"] = float("nan")
				self.data["target_prec"] = float("nan")
				self.data["dis_perc"] = float("nan")
				self.data["t_d"] = float("nan")
				self.data["ds_direction"] = "-"
			self.data["name"] = validSample.getCorrectWord
			data = validSample.getData
			self.data["tar_bef_ms"] = data["tar_bef_ms"]
			self.data["dis_bef_ms"] = data["dis_bef_ms"]
			self.data["tag_aft_ms"] = data["tag_aft_ms"]
			self.data["dis_aft_ms"] = data["dis_aft_ms"]
			self.data["before_total"] = data["before_total"]
			self.data["after_total"] = data["after_total"]
			self.data["tar_bef_perc"] = data["tar_bef_perc"]
			self.data["dis_bef_perc"] = data["dis_bef_perc"]
			self.data["target_prec"] = data["target_prec"]
			self.data["dis_perc"] = data["dis_perc"]
			self.data["t_d"] = data["t_d"]
			self.data["ds_direction"] = "-" if data.direction_code == "-" else (data.direction_code -(1 - data.direction_code))
		else:
			self.data["name"] = fstSample.getCorrectWord()
			d1 = fstSample.getData()
			d2 = fstSample.getData()
			self.data["tar_bef_ms"] = _meanOfValidData(d1["tar_bef_ms"],d2["tar_bef_ms"])
			self.data["dis_bef_ms"] = _meanOfValidData(d1["dis_bef_ms"],d2["dis_bef_ms"])
			self.data["tag_aft_ms"] = _meanOfValidData(d1["tag_aft_ms"],d2["tag_aft_ms"])
			self.data["dis_aft_ms"] = _meanOfValidData(d1["dis_aft_ms"],d2["dis_aft_ms"])
			self.data["before_total"] = _meanOfValidData(d1["before_total"],d2["before_total"])
			self.data["after_total"] = _meanOfValidData(d1["after_total"],d2["after_total"])
			self.data["tar_bef_perc"] = _meanOfValidData(d1["tar_bef_perc"],d2["tar_bef_perc"])
			self.data["dis_bef_perc"] = _meanOfValidData(d1["dis_bef_perc"],d2["dis_bef_perc"])
			self.data["target_prec"] = _meanOfValidData(d1["target_prec"],d2["target_prec"])
			self.data["dis_perc"] = _meanOfValidData(d1["dis_perc"],d2["dis_perc"])
			self.data["t_d"] = _meanOfValidData(d1["t_d"],d2["t_d"])
			maxDirectionCode = 0
			direction_correct = 0
			if d1["direction_code"] != "-":
				maxDirectionCode += 1
				direction_correct += d1["direction_code"]
			if d2["direction_code"] != "-":
				maxDirectionCode += 1
				direction_correct += d2["direction_code"]
			self.data["ds_direction"] = (direction_correct -(maxDirectionCode -direction_correct)) / maxDirectionCode

	def __str__(self):
		keys = ["name","tar_bef_ms","dis_bef_ms","tag_aft_ms","dis_aft_ms","before_total","after_total","tar_bef_perc","dis_bef_perc","target_prec","dis_perc","t_d"]
		values = [self.data[k] for k in  keys]
		return ("\n".join(values) + "\t"*5 + self.data["ds_direction"] + "\n").replace("\tinf\t","\tnan\t")



createVPsAndTables("../data_Rohdaten","../data_output/tables")