#reimplematition of parse_ms_4.py in julia=>
## needed to read in tsv file
using DataFrames
#define AOIS=>
aois = Dict(   ""=> [-1, -1], 
   "B_ESSEN_bauen_mögen1_neu.avi"=> [223, 1006],
   "B_essen_BAUEN_mögen2_neu.avi"=> [223, 1006],
   "A_BAUEN_essen_können1_neu.avi"=> [137, 1021],
   "A_bauen_ESSEN_können2_neu.avi"=> [137, 1021],
   "B_BADEN_fahren_mögen1_neu.avi"=> [226, 1030],
   "B_baden_FAHREN_mögen2_neu.avi"=> [226, 1030],
   "A_FAHREN_baden_können1_neu.avi"=> [189, 993],
   "A_fahren_BADEN_können2_neu.avi"=> [189, 993],
   "A_LESEN_anziehen_können1_neu.avi"=> [176, 1023],
   "A_lesen_ANZIEHEN_können2_neu.avi"=> [176, 1023],
   "B_ANZIEHEN_lesen_mögen1_neu.avi"=> [147, 1093],
   "B_anziehen_LESEN_mögen2_neu.avi"=> [147, 1093],
   "A_SCHAUKELN_trinken_können1_neu.avi"=> [151, 1041],
   "A_schaukeln_TRINKEN_können2_neu.avi"=> [151, 1041],
   "B_TRINKEN_schaukeln_mögen1_neu.avi"=> [213, 1044],
   "B_trinken_SChAUKELN_mögen2_neu.avi"=> [213, 1044],   
   "A_SITZEN_schlafen_können1_neu.avi"=> [207, 993],
   "A_sitzen_SCHLAFEN_können2_neu.avi"=> [207, 993],
   "B_SCHLAFEN_sitzen_mögen1_neu.avi"=> [170, 1026],
   "B_schlafen_SITZEN_mögen2_neu.avi"=> [170, 1026])

horizontal_window = [0, 960, 1920]
time_window = 3
result_file = open("results_csv.txt","w")
function fixation_in_window(data_table, line, area)
    left = 0
    right = 0
    #isna checks if the table value exsists is equivalent to pythons != ""
    if !isna(data_table[22][line])  && !isna(data_table[21][line]) 
        if aois[area][1] <= parse(Int,data_table[22][line]) <= aois[area][2]
            if horizontal_window[1] <= parse(Int,data_table[21][line]) < horizontal_window[2]
                                left = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
            end
            if horizontal_window[2] <= parse(Int,data_table[21][line]) < horizontal_window[3]
                                right = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
            end
        end
    else
        last_data = -2*time_window
        next_data = 2*time_window
        #changed range from 1-tw,0 to 1-tw,-1 because range border in julia is inclusive in contrast to python where the upper boarder is exclusiv.
        for i in [(1-time_window):-1...]
            if !isna(data_table[22][line + i])  && !isna(data_table[21][line + i]) 
                last_data = i
            end
            #access first column and count elements => numbers of rows
            if (line - i) < length(data_table[1]) && !isna(data_table[22][line - i])  && !isna(data_table[21][line - i])
                next_data = -i
            end
        end
        if (next_data - last_data) <= time_window
            if (aois[area][1] <= parse(Int,data_table[22][line + next_data]) <= aois[area][2]) &&
             (aois[area][1] <= parse(Int,data_table[22][line + last_data]) <= aois[area][2])
                if (horizontal_window[1] <= parse(data_table[21][line + next_data]) < horizontal_window[2]) &&
                 (horizontal_window[1] <= parse(Int,data_table[21][line + last_data]) < horizontal_window[2])
                    left = (parse(Int,data_table[10][line + 1]) - parse(Int,data_table[10][line]))
                end
                if (horizontal_window[2] <= parse(data_table[21][line + next_data]) < horizontal_window[3]) && (horizontal_window[2] <= parse(Int,data_table[21][line + last_data]) <horizontal_window[3])
                    right = (parse(Int,data_table[10][line+1]) - parse(Int,data_table[10][line]))
                end
            end
        end
    end
    return(left,right)
end

for file_name in readdir(pwd())
    if endswith(file_name,".tsv")
        write(result_file,"TSV-FILE\tAVI-File\tLeft_before\tLeft_After\tRight_Before\tRight_After\tFixation_Direction\tClick_time\n")
        data_table = readtable(file_name, separator = '\t',eltypes = repeat([String],outer = 27))
        for aoi in sort(collect(keys(aois)))
            start_line = 1
            end_line = 1
            for i in 1:length(data_table[1])
                if !isna(data_table[9][i])
                    if data_table[9][i] == aoi && start_line == 1
                        start_line = i
                    end
                    if data_table[9][i] == aoi
                        end_line = i
                    end
                end
            end
            last_click = -1
            for i in start_line:end_line
                if !isna(data_table[13][i])
                    last_click = i
                end
            end
            if last_click < 0
                write(result_file,"$file_name\t$aoi\tNo click registered\n")
            else
                left_before_click = 0
                right_before_click = 0
                click_time = parse(Int,data_table[10][last_click]) - parse(Int,data_table[10][start_line])
                # start collect data before click
                for i in start_line:(last_click-1)
                    (left,right) = fixation_in_window(data_table,i,aoi)
                    left_before_click += left
                    right_before_click += right
                end
                # Start collect data after click + 367 ms
                left_after_click = 0
                right_after_click = 0
                after_click_plus_duration = last_click
                while (parse(Int,data_table[10][after_click_plus_duration]) < (parse(Int,data_table[10][last_click]) +367))
                    after_click_plus_duration += 1
                end
                for i in after_click_plus_duration:(end_line-1)
                    (left,right) = fixation_in_window(data_table,i,aoi)
                    left_after_click += left
                    right_after_click += right
                end
                # Get the direction of the first registered fixation after the keypress:
                direction_fixation = ""
                first_fixation = after_click_plus_duration
                while first_fixation < end_line
                    while ((first_fixation < end_line) && (isna(data_table[21][first_fixation]) || isna(data_table[22][first_fixation])))
                        first_fixation += 1
                    end
                    if !isna(data_table[21][first_fixation]) && !isna(data_table[22][first_fixation])
                        if aois[aoi][1] <= parse(Int,data_table[22][first_fixation]) <= aois[aoi][2]
                                if 0 <= parse(Int,data_table[21][first_fixation]) < 960
                                    direction_fixation = "left"
                                    break
                                elseif 960 <= parse(Int,data_table[21][first_fixation]) < 1920
                                    direction_fixation = "right"
                                    break
                                else
                                    first_fixation += 1
                                end
                        else
                            first_fixation += 1
                        end
                    end
                end
                write(result_file,"$file_name\t$aoi\t$left_before_click\t$left_after_click\t$right_before_click\t$right_after_click\t$direction_fixation\t$click_time\n")
            end
        end
        write(result_file,"\n")
    end
end
close(result_file)

