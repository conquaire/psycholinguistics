# Psycholinguistics (Katharina Rohlfing)     

**Discipline:** Linguistics    

**Description from Proposal:**
(Child language acquisition):} PD Dr. Katharina Rohlfing studies language acquisition of young children by investigating mother-child interaction during a game and a free play situation. Recorded data includes audio and video recordings from 3 camera perspectives, which will be extensively coded and transcribed. CONQUAIRE will support the sharing of the data with a partner group from the University of Warsaw, Poland, who will act as external validator of the data.

**Student Assistant:** Samuel Cosper <scosper@campus.uni-paderborn.de>

## Interviews
* [Interview general](interview_general)
* [Interview 001](interview_001)
