# Interview (general)


## How large is the group (Head + Postdoc + PhD + Employees)


## What is the research subject / the phenomenon under study / the research question of the group’s research?


## What type measurements / experiments are made?
- [ ] Controlled / Open experiment
- [ ] Lab / Field
- [ ] Simulations
- [ ] Questionnaires
- [ ] other: ...


## What types of data are measured / generated?


## Which hardware + software (proprietary or open?) is used for measurements?


## What data formats are used? 
- [ ] proprietary or open?
- [ ] documented or undocumented


## What data volume is handled? (per Unit / experiment)


## What kinds of data analysis are made?


## Which software is used for data analysis? (proprietary or open?)


## Are there quality checks of the data? What kinds?


## Is there a plan / policies for archiving data?


## Is data being published?
- [ ] Have you? Where?
- [ ] Are you allowed?
- [ ] Would you?
- [ ] What is discipline-specific?


## Are evaluation scripts being published?
- [ ] Have you? Where?
- [ ] Are you allowed?
- [ ] Would you?
- [ ] What is discipline-specific?


## If applicable: Where did  you already publish data  software?
- [ ] PUB
- [ ] Github / Bitbucket / ... 
- [ ] Journal's repository
- [ ] Institutional repository
- [ ] group homepage
- [ ] other: ...


## Is data versioning relevant? If it is published, reference is made to a specific version of the data?


## Does a plan for long-term archiving exist?


## Does the group have a data manager? 


## Is there a need for support in the topic of data management?


## Are there explicit (written) policies for data management?


## Are there experiences with data management plans?


## Has data management been a topic in the context of funding proposals? 


## How important is documentation? Are there policies / best practices? (Metadata vocabularies?) 


## Have you ever tried to reproduce results by others? What were the results?


## How reproducible is work in your discipline? Why is that so?


## How important is reproducibility in your discipline?


## Do you work with personal data, i.e. is privacy protection necessary?


## Are there industrial co-operations that raise copyright issues


## Do / did you use data by other groups?


## Can you get data data from other researchers? How easy is it?
- [ ] very easy
- [ ] easy
- [ ] difficult
- [ ] extremely difficult


## Have you ever tried to get data from other groups? What was the result?
Was the data obtained from other researchers clean and ready-to-use? Or did you have to clean /adapt it for your research objectives?


## Are there any discipline-specific best practices for data / publication? How often is data published in your discipline?


## Are there any data or software repositories for your discipline?
- [ ] yes
- [ ] no
- [ ] don’t know


## Are they used?
- [ ] no
- [ ] sometimes
- [ ] often


## Do you have collaborations with other groups outside the university?


## Do you have cooperations with other groups within the university?


## What is your group’s overall IT technical affinity?
- [ ] rather low
- [ ] medium
- [ ] high
- [ ] very high


## What functionality would you expect from a university-wide platform for data management (please rate):
- [ ] long-term storage / archiving
- [ ] versioning control
- [ ] support collaborative work with other researchers
- [ ] automatic quality control of data
- [ ] publication of data
- [ ] linking of data with data from other researchers
- [ ] support for annotation and documentation
- [ ] other: ...


## Additional observations, comments and information

