#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# 4/27/2015, mschilli@techfak.uni-bielefeld.de
# Changed to UTF8 encoding (german umlaute)

# AOIs definieren
"""
This is a copy from the parse_ms_4.py script, which was provided by the psychologists, with documentation added.

The script reads the raw eye tracker data, which is stored in tsv files in the same folder.
For each AVI-file, which is given in the aois dict, it determines on which side the child has looked.
It also calculates left_before, right_before, left_after, right_after and click time. Then it stores alls this values for grouped by avi, to the output result_csv.txt file.
"""
aois = {
   "": [-1, -1], 
   "B_ESSEN_bauen_mögen1_neu.avi": [223, 1006],
   "B_essen_BAUEN_mögen2_neu.avi": [223, 1006],
   "A_BAUEN_essen_können1_neu.avi": [137, 1021],
   "A_bauen_ESSEN_können2_neu.avi": [137, 1021],
   "B_BADEN_fahren_mögen1_neu.avi": [226, 1030],
   "B_baden_FAHREN_mögen2_neu.avi": [226, 1030],
   "A_FAHREN_baden_können1_neu.avi": [189, 993],
   "A_fahren_BADEN_können2_neu.avi": [189, 993],
   "A_LESEN_anziehen_können1_neu.avi": [176, 1023],
   "A_lesen_ANZIEHEN_können2_neu.avi": [176, 1023],
   "B_ANZIEHEN_lesen_mögen1_neu.avi": [147, 1093],
   "B_anziehen_LESEN_mögen2_neu.avi": [147, 1093],
   "A_SCHAUKELN_trinken_können1_neu.avi": [151, 1041],
   "A_schaukeln_TRINKEN_können2_neu.avi": [151, 1041],
   "B_TRINKEN_schaukeln_mögen1_neu.avi": [213, 1044],
   "B_trinken_SChAUKELN_mögen2_neu.avi": [213, 1044],   
   "A_SITZEN_schlafen_können1_neu.avi": [207, 993],
   "A_sitzen_SCHLAFEN_können2_neu.avi": [207, 993],
   "B_SCHLAFEN_sitzen_mögen1_neu.avi": [170, 1026],
   "B_schlafen_SITZEN_mögen2_neu.avi": [170, 1026]
   }

"""
This dict stores for each avi file the vertical area of interess.
"""
# CUSTOMIZATION PARAMETERS
# Define left and right margins for looking windows: 
# [ left_border, central_line, right_border]
horizontal_window = [0, 960, 1920]
"""
List values are left boarder, central_line, right boarder
"""

time_window = 3
"""
 sliding time window - as often data points are missing we defined a short time
 window during which it is searched for the next fixation data:
 Data is only used when the fixation before the missing data point and after the missing
 data point is pointing into the same direction. Is given in number of steps (3 = 50 ms)
"""

# Read out list of lists of tab separated file:
# data_table[line of entry - 0 is the header line] [column]
import csv, os

# Open the result file
result_file = open('results_csv.txt','w')



def fixation_in_window(data_table, line, area):
    """
    Function which calculates how long the the probant looks right or left for the given line, in the given time window.
    Args:
        data_table: The raw data from Eyetracker
        line: the line where to check if the probant looks to left or right.
        area: the key of the aoi for the current line.
    Returns:
        returns a tuple with the time the probant looks right or left.

    """
    left = 0
    right = 0
    if data_table[line][21] != '' and data_table[line][20] != '':
        if aois[area][0] <= int(data_table[line][21]) <= aois[area][1]:
            if horizontal_window[0] <= int(data_table[line][20]) < horizontal_window[1]:
                left = (int(data_table[line+1][9]) - int(data_table[line][9]))
            if horizontal_window[1] <= int(data_table[line][20]) < horizontal_window[2]:
                right = (int(data_table[line+1][9]) - int(data_table[line][9]))
    # Check in the time_window if data is just dropped for a short time
    else:
        # Look at time before the current time step and see if data is available
        last_data = -2*time_window
        next_data = 2*time_window
        for i in range((1-time_window), 0):
            if data_table[line + i][21] != '' and data_table[line + i][20] != '':
                last_data = i   
            if (line-i)<len(data_table) and data_table[line - i][21] != '' and data_table[line - i][20] != '':
                next_data = -i
        if (next_data - last_data) <= time_window:
            if (aois[area][0] <= int(data_table[line+next_data][21]) <= aois[area][1]) and \
                    (aois[area][0] <= int(data_table[line+last_data][21]) <= aois[area][1]):
                if (horizontal_window[0] <= int(data_table[line+next_data][20]) < horizontal_window[1]) and \
                        (horizontal_window[0] <= int(data_table[line+last_data][20]) < horizontal_window[1]):
                    left = (int(data_table[line+1][9]) - int(data_table[line][9]))
                if (horizontal_window[1] <= int(data_table[line+next_data][20]) < horizontal_window[2]) and \
                        (horizontal_window[1] <= int(data_table[line+last_data][20]) < horizontal_window[2]):
                    right = (int(data_table[line+1][9]) - int(data_table[line][9]))
    return(left, right)

#
#the part below readins in all .tsv files from current folder. For each avifile it determines where the last click was, the time the proband looked left before the click and right before the click.
#it also calculates the time the proband looks left or right after the click and the first fixation after the click and writes this values to a result.tsv file.
#for more information look at the corresponding julia reimplementation.
#
for file_name in os.listdir(os.getcwd()):
    if file_name.endswith(".tsv"): 
        result_file.write("TSV-FILE \tAVI-File \tLeft_before \tLeft_After \tRight_Before \tRight_After \tFixation_Direction \tClick_time\n")
        data_table = list(csv.reader(open(file_name), delimiter='\t'))
    
        # Iterate over the different cases as defined in aois
        for aoi in sorted(aois):
            start_line = 0
            end_line = 0
            # Get the data for one aoi experiment
            for i in range(0,len(data_table)):
                if data_table[i][8]==aoi and start_line == 0:
                    start_line = i
                if data_table[i][8]==aoi:
                    end_line = i
            # find the last click
            last_click = -1
            for i in range(start_line, end_line):
                if data_table[i][12]!='':
                    last_click = i
            if last_click < 0:
                result_file.write(file_name + "\t" + aoi + "\t" + "No click registered\n")
            else:
                left_before_click = 0
                right_before_click = 0
                
                click_time = int(data_table[last_click][9]) - int(data_table[start_line][9])
                #print(aoi, " - ", click_time)
                
                # Start collect data before click:
                for i in range(start_line, last_click):
                    # Gaze is in the y window
                    (left, right) = fixation_in_window(data_table, i, aoi)
                    left_before_click += left
                    right_before_click += right
                        
                # Start collect data after click + 367 ms
                left_after_click = 0
                right_after_click = 0
                after_click_plus_duration = last_click
                while (int(data_table[after_click_plus_duration][9]) < (int(data_table[last_click][9]) + 367)):
                    after_click_plus_duration += 1
                for i in range(after_click_plus_duration, end_line):
                    # Gaze is in the y window
                    (left, right) = fixation_in_window(data_table, i, aoi)
                    left_after_click += left
                    right_after_click += right
                        
                # Get the direction of the first registered fixation after the keypress:
                direction_fixation = ""
                first_fixation = after_click_plus_duration
                while (first_fixation < end_line):
                    while ( (first_fixation < end_line) and ((data_table[first_fixation][20] == '') or (data_table[first_fixation][21] == '')) ) :
                        first_fixation += 1
                    if ((data_table[first_fixation][20] != '') and (data_table[first_fixation][21] != '')):
                        if aois[aoi][0] <= int(data_table[first_fixation][21]) <= aois[aoi][1]:
                            if 0 <= int(data_table[first_fixation][20]) < 960:
                                direction_fixation = "left"
                                break
                            elif 960 <= int(data_table[first_fixation][20]) < 1920:
                                direction_fixation = "right"
                                break
                            else:
                                first_fixation += 1
                        else:
                            first_fixation += 1
                result_file.write(file_name + "\t" + aoi + "\t" + str(left_before_click) + "\t" + str(left_after_click) + "\t" + str(right_before_click) + "\t" + str(right_after_click) + "\t" + str(direction_fixation) + "\t" + str(click_time) + "\n")
        result_file.write("\n")

result_file.close() 